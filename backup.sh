#!/bin/bash

# usage: export ANSIBLE_CONFIG=/applis/hadp/adminPHAD02/ansible/ansible.cfg
# usage: ansible Infrastructure -b -m shell -a "df -h /applis/repos/kerberos-upgrade-backup-tve"
# usage: ansible phadlx699 -b -m shell -a "ls -ltrh /applis/repos/kerberos-upgrade-backup-tve"
# usage: ansible phadlx699 -b -m shell -a "rm /applis/repos/kerberos-upgrade-backup-tve/*200929*"
# usage: ansible phadlx699 -b -m script -a 'backup.sh PHAD /applis/repos/kerberos-upgrade-backup-tve backup-ldap backup-ldap-ldif'
# usage: ansible Infrastructure -b -m script -a 'backup.sh PHAD /applis/repos/kerberos-upgrade-backup-tve backup-ldap backup-ldap-ldif'
 
# usage: export ANSIBLE_CONFIG=/applis/hadp/adminPHAD01/ansible/ansible.cfg
# usage: ansible  phadlx199 -b -m script -a 'backup.sh PHAD /applis/repos/kerberos-upgrade-backup-tve backup-ldap backup-kerberos backup-kerberos-principal backup-ldap-ldif'
# usage: ansible  phadlx198 -b -m script -a 'backup.sh PHAD /applis/repos/kerberos-upgrade-backup-tve backup-ldap backup-ldap-ldif'

#usage: sudo ./backup.sh PHAD /home/X195076 backup-ldap-ldif
[ $# -lt 3 ] && echo 'please provide realm, backupdir and action(s) ...' && exit 0

REALM=$1
shift
backupdir=$1
shift

[ ! -f /var/kerberos/krb5kdc/.k5.$REALM ] && echo 'invalid realm...' && exit 0
[ ! -d $backupdir ] && echo 'invalid backup dir...' && exit 0
echo "realm: $REALM, backupdir: $backupdir"

timestamp=$(date +'%y%m%d-%H%M')
currdir=$(pwd)
loguser=$(logname)
#homedir=$(getent passwd $loguser | cut -d: -f6)
thishost=$(hostname -s)

#backupdir=$homedir/myfiles
#backupdir=/bases/pgsql/dhadiha2/wal/ol-krb-backups

backup-ldap() {
  cd /var/lib; zip -r $backupdir/$thishost-$timestamp-var-lib-ldap ldap; chown $loguser $backupdir/$thishost-$timestamp-var-lib-ldap.zip
  cd /etc; zip -r $backupdir/$thishost-$timestamp-etc-openldap openldap; chown $loguser $backupdir/$thishost-$timestamp-etc-openldap.zip
  echo "${FUNCNAME[0]} done"
}

backup-kerberos() {
  cd /var; zip -r $backupdir/$thishost-$timestamp-var-kerberos kerberos; chown $loguser $backupdir/$thishost-$timestamp-var-kerberos.zip
  echo "${FUNCNAME[0]} done"
}

backup-kerberos-principal() {
  cp -p /var/kerberos/krb5kdc/.k5.$REALM $backupdir/$thishost-$timestamp-k5.$REALM; chown $loguser $backupdir/$thishost-$timestamp-k5.$REALM
  cp -p /var/kerberos/krb5kdc/principal $backupdir/$thishost-$timestamp-principal; chown $loguser $backupdir/$thishost-$timestamp-principal
  cp -p /var/kerberos/krb5kdc/principal.ok $backupdir/$thishost-$timestamp-principal.ok; chown $loguser $backupdir/$thishost-$timestamp-principal.ok
  echo "${FUNCNAME[0]} done"
}

backup-ldap-ldif() {
  slapcat -n0 -o ldif-wrap=no > $backupdir/$thishost-$timestamp-n0.ldif; chown $loguser $backupdir/$thishost-$timestamp-n0.ldif
  slapcat -n2 -o ldif-wrap=no > $backupdir/$thishost-$timestamp-n2.ldif; chown $loguser $backupdir/$thishost-$timestamp-n2.ldif

}

[ "$#" == "0" ] && echo 'please provide action ...' && exit 0

while (( "$#" )); do
  eval "$1"
  shift
done

ls -ltra $backupdir
echo "$backupdir"

