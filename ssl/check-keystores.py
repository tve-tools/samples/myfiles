#!/usr/bin/env python
# -*- coding: utf-8 -*-
# usage: sudo ./check-keystores.py find.res $(hostname -f)
# usage: sudo ./check-keystores.py ansible.LxS66h toto titi hello

from __future__ import print_function

import sys
import os
import shlex
import subprocess
from datetime import datetime
from dateutil.relativedelta import relativedelta

infilelist = sys.argv.pop(1)
inhost = sys.argv.pop(1)
passwds = sys.argv
passwds.pop(0)
passwds.insert(0, '')
jkscount = 0
global filename

def filter_on_expiry_date(line):
  if 'Valid from' not in line:
    return False

  print("	".join([inhost, 'expiry', filename, line]), file = sys.stderr)
  return(compare_date(line))


def compare_date(line):
  threshold_date = datetime.strptime('Sep 30 00:00:01 2020 GMT', '%b %d %H:%M:%S %Y %Z')
  month_before = datetime.now() - relativedelta(months = 1)

  date_str = line[line.index('until:') + 7:]
  expiry_date = datetime.strptime(date_str, '%a %b %d %H:%M:%S %Z %Y')

  if month_before < expiry_date < threshold_date:
    return True
  else:
    return False

def cleanup_useful_info(arr):
#  if len(arr) < 10:
#    return arr

  new_arr = []
  for i, e in enumerate(arr):
    if 'Your keystore contains' in e:
      new_arr.append(e)
      continue

    if 'Valid from:' not in e:
      continue

    if compare_date(e):
#      if i - 2 >= 0: new_arr.append(arr[i - 2])
      if i - 1 >= 0: new_arr.append(arr[i - 1].strip())
      new_arr.append(e.strip())
      if i + 1 >= 0: new_arr.append(arr[i + 1].strip())

  new_arr.append('truncated... (' + str(len(arr)) + ')')
  return new_arr



def filter_useful_info(line):
#  if 'Issuer' in line or 'Owner' in line or 'Valid' in line or 'Signature algorithm name' in line:
  if 'Issuer' in line or 'Valid' in line or 'Signature algorithm name' in line or 'Your keystore contains' in line:
    return True

  return False

def process_output(out):
  useful_info = []
  if filter(filter_on_expiry_date, out.splitlines()):
#    useful_info = filter(filter_useful_info, out.splitlines())
    useful_info = cleanup_useful_info(filter(filter_useful_info, out.splitlines()))

  return(useful_info)
#  return(useful_info[:100])
#  if not "".join(useful_info).isspace():
#    print("	".join(useful_info))
  

def process_err(err):
  if err:
    arr = err.splitlines()
    i = 0
    while i < len(arr):
      if 'Enter keystore password' in arr[i] or 'WARNING WARNING WARNING' in arr[i]  or 'integrity of the information stored' in arr[i]  or 'In order to verify its integrity' in arr[i]  or 'you must provide your keystore password' in arr[i] :
        del(arr[i])
      else:
        i += 1

    return(arr[:3])
#    if not (" ".join(arr[:3])).isspace(): 
#      print("	".join(arr[:3]))




def process_keystore_file(line):
  global jkscount
  if not os.path.isfile(line):
    return

#  if 'microsoft' not in line:
#    return

  fnull = open(os.devnull, 'w')

  cmd = 'keytool -list -v -keystore ' + line
  jkscount += 1

#  print(cmd)
  err = None
  out = None
  rc = 1
  outdata = []
  for passwd in passwds:
    p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE, stdin = subprocess.PIPE)
    p.stdin.write(passwd + '\n')
#    rc = p.wait()
    out, err = p.communicate()
    rc = p.returncode

    if rc == 0:
      outdata = process_output(out)
      break

  errerr = None
  errout = None
  if rc != 0:
    if err: errerr = process_err(err)
    if out: errout = process_err(out)
  if not outdata:
    if errerr: outdata = errerr
    else: outdata = errout

  if outdata:
    outdata.insert(0, line)
    outdata.insert(0, inhost)
    print('	'.join(outdata))



with open(infilelist) as fd:
  for line in fd:
    filename = line.strip()
    process_keystore_file(filename)

print('	'.join([ inhost, 'jks-count', str(jkscount) ]))
