#!/bin/bash
# usage: ansible expired_nodes -i ~/.user.env/inventory.ini -l phadlx85.haas.socgen -m copy -a "src=./check-ssl-cert.py dest=/tmp mode=0555"
# usage: ansible expired_nodes -i ~/.user.env/inventory.ini -l phadlx85.haas.socgen -m script -b -a "./check-ssl-certs.sh" --timeout=20 -t ./ansible-out/
# usage: ansible hdp-kafkanode-1 -i /applis/hadp//adminPHAD01/ansible/inventories/PHAD01.ini -l phadlx40 -m script -b -a "./check-ssl-certs.sh {{ ansible_host }}"

#host=$1
this_host=$(hostname -f)

scriptname=$(basename ${0%.*})
ssl_out=.$scriptname-$$.out

analyse_output() {

#notAfter=Jan 27 13:47:21 2024 GMT

  OUT_FILE="$1" HOST="$2" PORT="$3" python - << END
import os
from datetime import datetime
from dateutil.relativedelta import relativedelta

out_filename = os.environ.get('OUT_FILE')
host = os.environ.get('HOST')
port = os.environ.get('PORT')
content = [host, port]
expiry_date = None
with open(out_filename, 'r') as f:
  for line in f:
    content.append(line.strip())
    if 'notAfter' in line:
      date_str = line.split('=')[1].strip()
      expiry_date = datetime.strptime(date_str, '%b %d %H:%M:%S %Y %Z')

threshold_date = datetime.strptime('Sep 30 00:00:01 2020 GMT', '%b %d %H:%M:%S %Y %Z')
month_before = datetime.now() - relativedelta(months = 1)

if expiry_date:
  if month_before < expiry_date < threshold_date:
    print("	".join(content))
else:
  print("	".join(content))



END

}

## ###########
## check all  ports
## ###########
#for port in $(netstat -nlp |grep LISTEN |egrep -v 'STREAM|unix' |tr -s ' ' |cut -d ' ' -f 4|grep -o '[^:]*$' |sort -u -n)
##for port in 50475 44568
#do
#  timeout 5 openssl s_client -connect $this_host:$port < /dev/null 2>&1 | openssl x509 -noout -dates -issuer -subject >$ssl_out 2>&1
#  analyse_output $ssl_out $this_host $port
#  rm -f $ssl_out  2>/dev/null
#done

# ###########
# find all files
# ###########
find_root=/.
#sudo find $find_root -type d \( \
find $find_root -type d \( \
    -path $find_root/data1 \
    -o -path $find_root/data2 \
    -o -path $find_root/data3 \
    -o -path $find_root/data4 \
    -o -path $find_root/data5 \
    -o -path $find_root/data6 \
    -o -path $find_root/data7 \
    -o -path $find_root/data8 \
    -o -path $find_root/data9 \
    -o -path $find_root/data10 \
    -o -path $find_root/data11 \
    -o -path $find_root/data12 \
    -o -path $find_root/data13 \
    -o -path $find_root/data14 \
    -o -path $find_root/data15 \
    -o -path $find_root/data16 \
    -o -path $find_root/data17 \
    -o -path $find_root/data18 \
    -o -path $find_root/data19 \
    -o -path $find_root/data20 \
    -o -path $find_root/data21 \
    -o -path $find_root/data22 \
    -o -path $find_root/data23 \
    -o -path $find_root/data24 \
    -o -path $find_root/usr/share/doc \
    -o -path $find_root/usr/hdp/2.6.4.0-91/hive/doc \
    -o -path $find_root/usr/hdp/2.4.2.0-258/hive/doc \
    -o -path $find_root/usr/hdp/2.6.4.0-91/hive2/doc \
    -o -path $find_root/dev \
    -o -path $find_root/run \
    -o -path $find_root/tmp \
    -o -path $find_root/applis/splunk/produits/8.0.3/var/run/splunk \
    -o -path $find_root/proc \) -prune \
  -o -name "cacerts.txt" \
  -exec /tmp/check-ssl-cert.py {} $this_host \;


#  -o -name "*.jks" -o -name "*.pem" -o -name "*.key" -o -name "*.crt" 

###


#find_root=/.
#find $find_root -type d \( \
#    -path $find_root/data1 \
#    -o -path $find_root/data2 \
#    -o -path $find_root/data3 \
#    -o -path $find_root/data4 \
#    -o -path $find_root/data5 \
#    -o -path $find_root/data6 \
#    -o -path $find_root/data7 \
#    -o -path $find_root/data8 \
#    -o -path $find_root/data9 \
#    -o -path $find_root/data10 \
#    -o -path $find_root/data11 \
#    -o -path $find_root/data12 \
#    -o -path $find_root/data13 \
#    -o -path $find_root/data14 \
#    -o -path $find_root/data15 \
#    -o -path $find_root/data16 \
#    -o -path $find_root/data17 \
#    -o -path $find_root/data18 \
#    -o -path $find_root/data19 \
#    -o -path $find_root/data20 \
#    -o -path $find_root/data21 \
#    -o -path $find_root/data22 \
#    -o -path $find_root/data23 \
#    -o -path $find_root/data24 \
#    -o -path $find_root/usr/share/doc \
#    -o -path $find_root/usr/hdp/2.6.4.0-91/hive/doc \
#    -o -path $find_root/usr/hdp/2.4.2.0-258/hive/doc \
#    -o -path $find_root/usr/hdp/2.6.4.0-91/hive2/doc \
#    -o -path $find_root/dev \
#    -o -path $find_root/run \
#    -o -path $find_root/applis/splunk/produits/8.0.3/var/run/splunk \
#    -o -path $find_root/proc \) -prune \
#  -o -name "*.jks" \
#  -exec bash -c '[ -f {} ] && cksum {}' \;


