#!/usr/bin/env python
# -*- coding: utf-8 -*-

# usage: ./parse-ansible.py ansible-out-2 >all.csv

from __future__ import print_function
import json
import sys
import os

ansible_out_dir = sys.argv[1]

my_filter = ''

if len(sys.argv) > 2:
  my_filter = sys.argv[2]

for filename in sorted(os.listdir(ansible_out_dir)):
  if my_filter and my_filter not in filename:
    continue

  with open(ansible_out_dir + os.sep + filename) as json_file:
    data = json.load(json_file)
    
    if 'stdout' not in data:
      print(filename + '	host has generated error; please check...')
    else:
      for line in data['stdout'].splitlines():
        pass
        print(filename + "	" + line.encode('utf-8'))

