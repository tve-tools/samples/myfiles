#!/usr/bin/env python
# -*- coding: utf-8 -*-

# usage: ansible hdp-kafkanode-1 -i /applis/hadp//adminPHAD01/ansible/inventories/PHAD01.ini -l phadlx40 -m script -b -a "./read-ssl-files.py {{ ansible_host }}"

import subprocess
import shlex
import os
import sys
from datetime import datetime, date

host=sys.argv[1]
"""
arr = [ 
"/./etc/ipa/ca.crt",
"/./etc/openldap/cacerts/ca.pem"
]

"""
arr = [
"/./etc/ipa/ca.crt",
"/./etc/openldap/cacerts/ca.pem",
"/./etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt",
"/./etc/pki/ca-trust/extracted/pem/email-ca-bundle.pem",
"/./etc/pki/ca-trust/extracted/pem/objsign-ca-bundle.pem",
"/./etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem",
"/./etc/pki/ca-trust/source/anchors/katello-server-ca.pem",
"/./etc/pki/ca-trust/source/ca-bundle.legacy.crt",
"/./etc/pki/consumer/bundle.pem",
"/./etc/pki/consumer/cert.pem",
"/./etc/pki/consumer/key.pem",
"/./etc/pki/entitlement/1027173868566527262.pem",
"/./etc/pki/entitlement/1027173868566527262-key.pem",
"/./etc/pki/entitlement/1241808059252919482.pem",
"/./etc/pki/entitlement/1241808059252919482-key.pem",
"/./etc/pki/entitlement/1855149713407367441.pem",
"/./etc/pki/entitlement/1855149713407367441-key.pem",
"/./etc/pki/entitlement/1917148566455006195.pem",
"/./etc/pki/entitlement/1917148566455006195-key.pem",
"/./etc/pki/entitlement/2540874636235079448.pem",
"/./etc/pki/entitlement/2540874636235079448-key.pem",
"/./etc/pki/entitlement/2785489556467881206.pem",
"/./etc/pki/entitlement/2785489556467881206-key.pem",
"/./etc/pki/entitlement/3137989271060350185.pem",
"/./etc/pki/entitlement/3137989271060350185-key.pem",
"/./etc/pki/entitlement/3373011242174855395.pem",
"/./etc/pki/entitlement/3373011242174855395-key.pem",
"/./etc/pki/entitlement/3628049962261624678.pem",
"/./etc/pki/entitlement/3628049962261624678-key.pem",
"/./etc/pki/entitlement/3637376710437803583.pem",
"/./etc/pki/entitlement/3637376710437803583-key.pem",
"/./etc/pki/entitlement/3783643721674802590.pem",
"/./etc/pki/entitlement/3783643721674802590-key.pem",
"/./etc/pki/entitlement/4367172652499359013.pem",
"/./etc/pki/entitlement/4367172652499359013-key.pem",
"/./etc/pki/entitlement/4591989502591601762.pem",
"/./etc/pki/entitlement/4591989502591601762-key.pem",
"/./etc/pki/entitlement/5372426761277251168.pem",
"/./etc/pki/entitlement/5372426761277251168-key.pem",
"/./etc/pki/entitlement/5448966873882234618.pem",
"/./etc/pki/entitlement/5448966873882234618-key.pem",
"/./etc/pki/entitlement/5757581053436888745.pem",
"/./etc/pki/entitlement/5757581053436888745-key.pem",
"/./etc/pki/entitlement/6371324786878740743.pem",
"/./etc/pki/entitlement/6371324786878740743-key.pem",
"/./etc/pki/entitlement/6716566606345075206.pem",
"/./etc/pki/entitlement/6716566606345075206-key.pem",
"/./etc/pki/entitlement/7023034883585619161.pem",
"/./etc/pki/entitlement/7023034883585619161-key.pem",
"/./etc/pki/entitlement/744350967315740964.pem",
"/./etc/pki/entitlement/744350967315740964-key.pem",
"/./etc/pki/entitlement/7743562932296793983.pem",
"/./etc/pki/entitlement/7743562932296793983-key.pem",
"/./etc/pki/entitlement/7866879010634637449.pem",
"/./etc/pki/entitlement/7866879010634637449-key.pem",
"/./etc/pki/entitlement/8034576461683107331.pem",
"/./etc/pki/entitlement/8034576461683107331-key.pem",
"/./etc/pki/entitlement/8274486069668105171.pem",
"/./etc/pki/entitlement/8274486069668105171-key.pem",
"/./etc/pki/product/201.pem",
"/./etc/pki/product/69.pem",
"/./etc/pki/product-default/69.pem",
"/./etc/pki/tls/cert.pem",
"/./etc/pki/tls/certs/ca-bundle.crt",
"/./etc/pki/tls/certs/ca-bundle.trust.crt",
"/./etc/qualys/cloud-agent/cert/custom-ca.crt",
"/./etc/redhat-access-insights/api.access.redhat.com.pem",
"/./etc/redhat-access-insights/cert-api.access.redhat.com.pem",
"/./etc/rhsm/ca/katello-default-ca.pem",
"/./etc/rhsm/ca/katello-server-ca.pem",
"/./etc/rhsm/ca/redhat-uep.pem",
"/./etc/security/kafka/kafka.server.keystore.jks",
"/./etc/trusted-key.key",
"/./home/A454286/kafka.mosaic-gua.keystore.jks",
"/./home/X072703/kafka.bbcprod.keystore.jks",
"/./home/X072703/kafka.lrphmonitoring.keystore.jks",
"/./home/X072703/kafka.lrphmonitoring.keystore_test.jks",
"/./home/X178563/kafka.mosaic-gua.keystore.jks",
"/./home/X187335/kafka.bbcprdstream.keystore.jks",
"/./home/X187335/kafka.bbcprod.keystore.jks",
"/./home/X187335/kafka.bbcprodstream.keystore.jks",
"/./home/X187335/kafka.bbcpstream.keystore.jks",
"/./local/menupil/GENERAL/products/python2/Lib/test/allsans.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/badcert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/badkey.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/dh1024.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/keycert.passwd.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/keycert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/keycert2.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/keycert3.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/keycert4.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/nokia.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/nullbytecert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/nullcert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/pycacert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/selfsigned_pythontestdotnet.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/ssl_cert.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/ssl_key.passwd.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/ssl_key.pem",
"/./local/menupil/GENERAL/products/python2/Lib/test/wrongcert.pem",
"/./opt/auditvol/.ssl/public.pem",
"/./opt/auditvol/ca-cert.pem",
"/./opt/auditvol/client-cert.pem",
"/./opt/auditvol/client-key.pem",
"/./opt/auditvol/info.d/config.2020.06.14.08.33/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.08.52/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.09.58/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.13.46/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.17.28/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.18.12/etc/security/kafka/kafka.server.keystore.jks",
"/./opt/auditvol/info.d/config.2020.06.14.19.43/etc/security/kafka/kafka.server.keystore.jks",
"/./proc",
"/./produits/mirrormaker/hadpp.ok/ssl/hadpp.crt",
"/./produits/mirrormaker/hadpp.ok/ssl/hadpp.key",
"/./produits/mirrormaker/hadpp.ok/ssl/hadpp.keystore.jks",
"/./produits/mirrormaker/hadpp.ok/ssl/hadpp.truststore.jks",
"/./produits/mirrormaker/hadpp.ok/ssl/openssl/newcerts/01.pem",
"/./produits/mirrormaker/hadpp.serial/ssl/hadpp.crt",
"/./produits/mirrormaker/hadpp.serial/ssl/hadpp.key",
"/./produits/mirrormaker/hadpp.serial/ssl/hadpp.keystore.jks",
"/./produits/mirrormaker/hadpp.serial/ssl/hadpp.truststore.jks",
"/./produits/mirrormaker/hadpp.serial/ssl/openssl/newcerts/01.pem",
"/./usr/hdp/2.4.2.0-258/hive/doc",
"/./usr/hdp/2.6.4.0-91/hive/doc",
"/./usr/hdp/2.6.4.0-91/hive2/doc",
"/./usr/lib/crda/pubkeys/key.pub.pem",
"/./usr/lib/crda/pubkeys/sforshee.key.pub.pem",
"/./usr/share/doc",
"/./usr/share/pki/ca-trust-legacy/ca-bundle.legacy.default.crt",
"/./usr/share/pki/ca-trust-legacy/ca-bundle.legacy.disable.crt",
"/./usr/share/pki/ca-trust-source/ca-bundle.neutral-trust.crt",
"/./usr/share/pki/ca-trust-source/ca-bundle.trust.crt",
"/./var/lib/ambari-agent/keys/ca.crt",
"/./var/lib/ambari-agent/keys/phadlx42.haas.socgen.crt",
"/./var/lib/ambari-agent/keys/phadlx42.haas.socgen.key",
"/./var/lib/ambari-agent/keys/phadlx43.haas.socgen.crt",
"/./var/lib/ambari-agent/keys/phadlx43.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/ca.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx40.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx40.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx41.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx41.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx42.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx42.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx43.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx43.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx44.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx44.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx45.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx45.haas.socgen.key",
"/./var/lib/smartsense/hst-agent/keys/phadlx46.haas.socgen.crt",
"/./var/lib/smartsense/hst-agent/keys/phadlx46.haas.socgen.key",
"/./var/lib/smartsense/hst-common/anonymization/keys/private_anonymization.key",
"/./var/lib/smartsense/hst-common/anonymization/keys/shared_anonymization.key",
"/./var/lib/smartsense/hst-common/encryption/keys/public.key",
"/./var/lib/smartsense/hst-common/encryption/keys/public_dev.key",
"/./var/lib/smartsense/hst-gateway/truststore/smartsense.jks",
"/./var/lib/smartsense/hst-gateway/truststore_09_08_18_11_43.save/smartsense.jks",
"/./var/opt/managesoft/etc/ssl/cert.pem",
"/./var/opt/opsware/crypto/agent/admin-ca.crt",
"/./var/opt/opsware/crypto/agent/agent-ca.crt",
"/./var/opt/opsware/crypto/agent/opsware-ca.crt"
]
#"""

def filter_openssl_lines(line):
  if 'Not After' in line:
    return True

  if 'Signature Algorithm' in line:
    return True

  if 'Issuer:' in line:
    return True

  return False

def filter_not_after(line):
  if 'Not After' not in line:
    return False

  expiry_date = datetime. strptime('Aug 15 00:00:01 2020 GMT', '%b %d %H:%M:%S %Y %Z')
  date_str = line[line.index('GMT') - 21:line.index('GMT') + 3]
  date = datetime. strptime(date_str, '%b %d %H:%M:%S %Y %Z')

  today = date.today()

  if date > today and date < expiry_date:
    return True
  else:
    return False

#  year = line[-8:]
#  year = year[:4]
##  print(year)
#  if int(year) > 2020:
#    return False
#  else:
#    return True


for filename in arr:
  if 'haas.socgen' in filename and host not in filename:
    continue

  if not os.path.isfile(filename):
#    print("is not file: " + filename)
    continue

  if '.jks' in filename:
#    print('jks -- ' + filename)
    continue

#  cmd = "openssl x509 -text -noout -in " + filename + " |egrep 'Not After|Signature Algorithm|Issuer:'"
#  print('this is the cmd: ' + cmd)
#  if os.system(cmd) != 0:
#    print('failed: ' + filename)

  cmd = "openssl x509 -text -noout -in " + filename
  p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE)
  out = p.stdout.read()
  out_arr = out.splitlines()
  err = p.stderr.read()

  for line in filter(filter_not_after, out_arr):
    print(filename)
    for i in filter(filter_openssl_lines, out_arr):
      print(i)



