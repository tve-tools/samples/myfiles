# jks
```bash
ansible-playbook -l expired_nodes -i ~/.user.env/inventory.ini check-keystores.yml
./parse-ansible2.py check-keystores-out >expired_nodes-jks.csv

```

# pem, crt
```bash
ansible expired_nodes -i ~/.user.env/inventory.ini -m copy -a "src=./check-ssl-cert.py dest=/tmp mode=0555"
ansible expired_nodes -i ~/.user.env/inventory.ini -l phadlx54.haas.socgen -m script -b -a "./check-ssl-certs.sh" --timeout=20 -t check-ssl-certs-out
./parse-ansible.py check-ssl-certs-out >expired_nodes-pem-crt.csv

```
