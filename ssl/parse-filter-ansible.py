#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import json
import sys
import os

ansible_out_dir = sys.argv[1]

thisdict = {}

filetype = ''
if len(sys.argv) > 2:
  filetype = sys.argv[2]

my_filter = ''
if len(sys.argv) > 3:
  my_filter = sys.argv[3]

def process_jks(host, json_data):
  if 'stdout' not in data:
    print(host + '	host has generated error; please check...')
  else:
    for line in data['stdout'].splitlines():
      elmts = line.split()
      key = elmts[0] + ':' + elmts[1]
#      print(key)
      if key not in thisdict:
        thisdict[key] = host + "  " + line
        
#    for elmt in thisdict.values():
#      print(host + "	" + elmt.encode('utf-8'))



for filename in sorted(os.listdir(ansible_out_dir)):
  if my_filter and my_filter not in filename:
    continue

  with open(ansible_out_dir + os.sep + filename) as json_file:
    data = json.load(json_file)
    
    if filetype and 'jks' in filetype:
      process_jks(filename, data)

for elmt in thisdict.values():
 print(elmt.encode('utf-8'))

