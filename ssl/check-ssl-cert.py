#!/usr/bin/env python
# -*- coding: utf-8 -*-
# usage: ansible expired_nodes -i ~/.user.env/inventory.ini -m copy -a "src=./check-ssl-cert.py dest=/tmp mode=0555"

import subprocess
import shlex
import os
import sys
from datetime import datetime
from dateutil.relativedelta import relativedelta

global filename
global host

def filter_openssl_lines(line):
  if 'Not After' in line:
    return True

  if 'Signature Algorithm' in line:
    return True

  if 'Issuer:' in line:
    return True

  return False


def filter_on_expiry_date(line):
  if 'Not After' not in line:
    return False

  print('\t'.join( [host, 'expiry', filename]))
  threshold_date = datetime.strptime('Sep 30 00:00:01 2020 GMT', '%b %d %H:%M:%S %Y %Z')

  date_str = line[line.index('GMT') - 21:line.index('GMT') + 3]
  expiry_date = datetime.strptime(date_str, '%b %d %H:%M:%S %Y %Z')

  month_before = datetime.now() - relativedelta(months = 1)

  if month_before < expiry_date < threshold_date:
    return True
  else:
    return False


try:
  filename = sys.argv[1]
  host = sys.argv[2]
except:
  print('provide input file to be processed...')
  sys.exit(1)


if not filename:
  print('provide input file to be processed...')
  sys.exit(1)

if 'haas.socgen' in filename and host not in filename:
  sys.exit(2)

if not os.path.isfile(filename):
  sys.exit(3)

if '.jks' in filename:
#    print('jks -- ' + filename)
  sys.exit(4)

cmd = "openssl x509 -text -noout -in " + filename
p = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr = subprocess.PIPE)
out, err = p.communicate()
#out = p.stdout.read()
out_arr = out.splitlines()
#err = p.stderr.read()


#for due_to_expire in filter(filter_on_expiry_date, out_arr):
if filter(filter_on_expiry_date, out_arr):
  printout = [ filename ]
  sigAlgAlreadyDisp = False
  for line in filter(filter_openssl_lines, out_arr):
    if not 'Signature Algorithm' in line: 
      printout.append(line.strip())
    else:
      if not sigAlgAlreadyDisp:
        printout.append(line.strip())
        sigAlgAlreadyDisp = True
  print("	".join(printout))



