from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks import SSHHook
import pprint
import time


dag = DAG('dbebpprdk8slx6659_kinit',
          description='dbebpprdk8slx6659_kinit',
          schedule_interval=None,
          start_date=datetime(2021, 2, 1),
          catchup=False,
          params={
            "krb_cache" : 'KRB5CCNAME=$HOME/.yarn-yars-jobs-ksession',
            "keytab_file":"/home/X195076/.safe/smokeuser.headless.keytab" ,
            "kerberos_principal":"ambari-qa-tve05" 
          }
)

# Step 0 - execute launch of first check
t1_bash = """
touch /tmp/airflow-ssh-check
"""
t1 = SSHOperator(
    ssh_conn_id = 'ssh_X195076_dbebpprdk8slx6659',
    task_id = 'shell_launcher_1',
    command = t1_bash,
#    do_xcom_push = True,
    dag = None)
#    dag = dag)

# Step 1 - check krb session
check_krb = '{{ params.krb_cache }} klist -s'
t_check_krb = SSHOperator( dag = dag, ssh_conn_id = 'ssh_X195076_dbebpprdk8slx6659', 
  task_id = 't_check_krb', 
  command = check_krb )

# Step 2 - kinit 
kinit = '{{ params.krb_cache }} kinit -kt {{ params.keytab_file }} {{ params.kerberos_principal }} '
t_kinit = SSHOperator( dag = dag, ssh_conn_id = 'ssh_X195076_dbebpprdk8slx6659',
    task_id = 't_kinit',
    command = kinit,
    trigger_rule = TriggerRule.ALL_FAILED )

# Step 3 - get last Yars (sparkpi) Yarn job
get_yarn_app = """
{{ params.krb_cache }} yarn application -list -appStates ALL 2>/dev/null |grep org.apache.spark.examples.SparkPi |sed -e 's:\s.*::' |sort  |tail -1
"""
t_get_yarn_app = SSHOperator( dag = dag, ssh_conn_id = 'ssh_X195076_dbebpprdk8slx6659', do_xcom_push = True, 
  task_id = 't_get_yarn_app', 
  command = get_yarn_app,
  trigger_rule = TriggerRule.ONE_SUCCESS )


# debug task
t_debug = BashOperator( dag = None, 
    task_id = 't_debug',
    bash_command = 'echo "{{ ti.xcom_pull(task_ids="t_get_yarn_app", key="return_value") }}"' )


# get yarn details
def get_yarn_details(**kwargs):
    appid = kwargs['ti'].xcom_pull(task_ids = 't_get_yarn_app', key="return_value").decode('utf-8')
    print('message: ' + appid)
    # pprint(kwargs['dag_run'].conf["description"])
    # pprint(kwargs['dag_run'].conf["params"])

    ssh = SSHHook(ssh_conn_id = 'ssh_X195076_dbebpprdk8slx6659')
    ssh_client = ssh.get_conn()
    stdin, stdout, stderr = ssh_client.exec_command('KRB5CCNAME=$HOME/.yarn-yars-jobs-ksession yarn application -status ' + appid)
  #  ssh_client.close()
    
    start_time = ''
    finish_time = ''
    for line in stdout:
      if 'Start-Time' in line: start_time = time.ctime(int(line.split()[2]))
      if 'Finish-Time' in line: finish_time = time.ctime(int(line.split()[2]))

    # pprint(stderr)
    for line in stderr:
      print(line)

    print('start time: ' + start_time)
    print('finish time: ' + finish_time)


t_get_yarn_details = PythonOperator( dag = dag, 
    task_id='t_get_yarn_details', 
    python_callable=get_yarn_details,
    provide_context=True )


# DAG definition
# t1 >> kinit_task
t_check_krb >> t_kinit
# [ t_check_krb, t_kinit ] >> t_get_yarn_app >> t_debug >> t_get_yarn_details
[ t_check_krb, t_kinit ] >> t_get_yarn_app >> t_get_yarn_details

