#!/bin/bash

# usage: 2-59/5 * * * * /home/hive/hive-jvm-info.sh >/home/hive/hive-jvm-info.log 2>&1

thishost=$(hostname -s)
logdir="/tmp/hive-jvm-info"

mkdir ${logdir} 2>/dev/null

zip-file() {
#  set -x
  filen=$1
  cd $logdir
  typeset int filesz
  filesz=$(ls -l $filen |tr -s ' ' |cut -d ' ' -f 5)
  [[ $filesz -gt 50000000 ]] && gzip -f $filen
  cd -
#  set +x
}

procid=$(< /var/run/hive/hive-server.pid)
#zip-file hs2-jmap-${thishost}.txt
date >> ${logdir}/hs2-jmap-${thishost}.txt 2>&1
echo "pid: $procid" >> ${logdir}/hs2-jmap-${thishost}.txt 2>&1
jmap -heap $procid  >> ${logdir}/hs2-jmap-${thishost}.txt 2>&1

zip-file hs2-jstack-${thishost}.txt
date >> ${logdir}/hs2-jstack-${thishost}.txt 2>&1
jstack -l $procid  >> ${logdir}/hs2-jstack-${thishost}.txt 2>&1


## hms
[ ! -f /var/run/hive/hive.pid ] && exit 0

procid=$(< /var/run/hive/hive.pid)
#zip-file hms-jmap-${thishost}.txt
date >> ${logdir}/hms-jmap-${thishost}.txt 2>&1
echo "pid: $procid" >> ${logdir}/hms-jmap-${thishost}.txt 2>&1
jmap -heap $procid  >> ${logdir}/hms-jmap-${thishost}.txt 2>&1

zip-file hms-jstack-${thishost}.txt
date >> ${logdir}/hms-jstack-${thishost}.txt 2>&1
jstack -l $procid  >> ${logdir}/hms-jstack-${thishost}.txt 2>&1

# ansible HIVE_METASTORE:HIVE_SERVER -m copy -o -b -a "src=hive-jvm-info.sh dest=/home/hive owner=hive group=hive mode=744"

# ansible HIVE_METASTORE:HIVE_SERVER -m shell -o -a 'echo "2-59/5 * * * * /home/hive/hive-jvm-info.sh >/home/hive/hive-jvm-info.log 2>&1" |crontab -' -b --become-user=hive

# ansible HIVE_METASTORE:HIVE_SERVER -m shell -o -a 'rm /var/spool/cron/hive' -b

