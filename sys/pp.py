#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

with open('/tmp/ansible-log.txt') as f:
  for line in f:
    line = line.strip()
    line = line.replace("'", '"')
    line = line.replace('False', '"False"')
#    print(line)
    jsondata = json.loads(line)
    print(json.dumps(jsondata, indent=2))

# json specs: double-quotes instead of single-quotes
# only one root element
# literal values in lower case: example False
# https://jsonformatter.curiousconcept.com/#
