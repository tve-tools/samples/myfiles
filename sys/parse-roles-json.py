#!/usr/bin/env /usr/bin/python
# -*- coding: utf-8 -*-

import sys
import yaml

def main(argv):
  print('start')
  print_yaml_file('roles.yml')




def print_yaml_file(filename):
  with open(filename) as f:
    yaml_data = yaml.load(f)

  role = yaml_data['kib_ied_bddf']
  for rolekey in yaml_data:
    print(rolekey + ' processing role: ' + rolekey)
    role = yaml_data[rolekey]

    for key in role:
      if isinstance(role[key], basestring):
        print(rolekey + '\t(value type basestring for ) ' + key)
        print(rolekey + '\t' + key + '\t' + role[key])
  
      elif isinstance(role[key], list):
        if key == 'indices':
          for idx in role[key]:
            if isinstance(idx['names'], list):
              print(rolekey + '\t' + ','.join(idx['names']) + '\t' + ','.join(idx['privileges']))
            else: 
              print(rolekey + '\t' + str(idx['names']) + '\t' + ','.join(idx['privileges']))
  
        elif key == 'cluster':
          print(rolekey + '\t' + key + '\t' + ','.join(role[key]))
  
        else:
          print(rolekey + '\t(key not processed) ' + key)
  
      else:
        print(rolekey + '\t(value type not detected for ) ' + key)
        print(rolekey + '\t' + key + '\t' + str(role[key]))
  



if __name__ == '__main__':
  main(sys.argv)


