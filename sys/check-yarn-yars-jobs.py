#!/usr/bin/env python2

import sys
import subprocess
import time

def main(argv):
  get_yarn_yars_jobs()

KRBSESSIONCACHE = 'KRB5CCNAME=$HOME/.yarn-yars-jobs-ksession '
KEYTABFILE = '/home/x195076/.safe/smokeuser.headless.keytab'
PRINC = 'ambari-qa-ihadcluster02@DIHAD'

def get_yarn_yars_jobs():
  if manage_krb_session() != 0:
    print('kerberos session not set')
    return

#  list_yarn_jobs_cmd = 'yarn application -list -appStates ALL |grep -i yars'
  list_yarn_jobs_cmd = KRBSESSIONCACHE + 'yarn application -list -appStates ALL'
  stdo, stde, rc = runcmd(list_yarn_jobs_cmd)
  if 'exception' in stde.lower() or 'err' in stde.lower() :
    for line in stde.splitlines(): print line
    return
  
  yars_jobs = []
  for line in filter(filter_yars_jobs, stdo):
    yars_jobs.append(line.split())
  print(yars_jobs[0])
  get_yarn_details(yars_jobs[0][0])


def get_yarn_details(appid):
  yarn_details_cmd = KRBSESSIONCACHE + 'yarn application -status ' + appid
  stdo, stde, rc = runcmd(yarn_details_cmd)
  if 'exception' in stde.lower() or 'err' in stde.lower() :
    for line in stde.splitlines(): print line
    return

  start_time = ''
  finish_time = ''
  for line in stdo: 
    print(line)
    if 'Start-Time' in line: start_time = time.ctime(int(line.split()[2]))
    if 'Finish-Time' in line: finish_time = time.ctime(int(line.split()[2]))
  
  print(start_time)
  print(finish_time)


def filter_yars_jobs(line):
  if 'yars' in line: return True
  else: return False

def manage_krb_session():
  krb_check_session_cmd = KRBSESSIONCACHE + ' klist -s'
  stdo, stde, rc = runcmd(krb_check_session_cmd)

  if rc == 0: return(0)

  print('need to setup krb session...')
  krb_set_session_cmd = KRBSESSIONCACHE + ' kinit -kt ' + KEYTABFILE + ' ' + PRINC
  stdo, stde, rc = runcmd(krb_set_session_cmd)
  if rc == 0:  return(0)

  print('cannot establish krb session...')
  return(1)


def runcmd(cmd):
  process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  output, error = process.communicate()
#  return(output.splitlines(), error.splitlines())
  return(output.splitlines(), error, process.returncode)


if __name__ == '__main__':
  main(sys.argv)


