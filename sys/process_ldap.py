#!/usr/bin/env /applis/hadd/miniconda3/bin/python
# -*- coding: utf-8 -*-

#
# usage: [hadpadm@phadlx198 ~]$ time ./process_ldap.py
#

import sys
import subprocess
import tempfile
from os.path import expanduser
import pandas as pd
import time
import os

USEFUL_FIELDS = [ 'dn', 'cn', 'memberOf', 'memberUid', 'uid', 'sn', 'mail' ]

def main(argv):
  slapcat_file = run_slapcat()
  print(slapcat_file)
#  slapcat_file = 'n2bak.ldif.2'
  df = load_slapcat(slapcat_file)
  os.remove(slapcat_file)

#  df = load_slapcat('slapcat.lite')

  df1 = get_users_with_memberof(df)
  df2 = get_users_from_group(df)
  merged = df1.merge(df2, how='outer', left_on='uid', right_on='uid')
  merged.to_csv('unified.csv',header=True,index=False)

#  while True:
#    time.sleep(2)
#    print('.', end = '', flush = True)


def get_users_with_memberof(df):
#  df['memberOf'] = df['memberOf'].astype('string')
  contain_values = df[df['memberOf'].str.contains('cn=gssh_i2t')]
#  contain_values = df[df['cn'].str.contains('Neji')]
#  contain_values.set_option('display.max_rows', None)
  required_cols = contain_values[['cn', 'sn', 'mail', 'uid']].sort_values('uid')
  required_cols.to_csv('users_props.csv',header=True,index=False)
  return(required_cols)


def get_users_from_group(df):
  contain_values = df[df['dn'].str.contains('cn=gssh_i2t')][['memberUid']]
  for column in contain_values:
    uids = contain_values[column].values[0].split(';;')
    uids_df = pd.DataFrame(uids, columns=['uid'])
#    for uid in uids:
#      print(uid)
    merged = df.merge(uids_df, how='right', left_on='uid', right_on='uid')[['cn', 'sn', 'mail', 'uid']].sort_values('uid')
#    print(merged)
    merged.to_csv('from_groups.csv',header=True,index=False)
    return(merged)


def run_slapcat():
  home = expanduser("~")
  f = tempfile.NamedTemporaryFile(mode='w+b', delete=False, dir=home, prefix='.process_ldap_', suffix='.ldif')

#  cmd = r'ls -l /tmp'
  cmd = r'sudo slapcat -o ldif-wrap=no'

  try:
    subprocess.run(cmd, shell=True, timeout = 120, stdout = f)
  except subprocess.CalledProcessError as e:
    print(e.output)
  f.close()

  return(f.name)


def load_slapcat(filename):
  df = pd.DataFrame(columns = USEFUL_FIELDS)
  df_counter = 0
  with open(filename) as fd:
    for line in fd:
      # do not load kerberos entries (in the case kerberos is using ldap as backend)
      if line.startswith('dn: krb'):
        read_fd_until_empty_line(fd)

      else:
        a = transpose_lines(line, fd)
        arr = []
        for field in USEFUL_FIELDS:
          # convert array to str (quick hack in order to be able to use pandas str.contains() above)
          if isinstance(a[field], list):
            arr.append(';;'.join(a[field]))
          else:
            arr.append(a[field])

        df.loc[df_counter] = arr
        df_counter += 1
#        print(a)
#  print(df)
  return(df)


def transpose_lines(line, fd):
  d = {}
  if line.rstrip().startswith('dn:'):
    d['dn'] = line.rstrip()[4:]
  else:
    print(f"{line} is not dn!")

  for line in fd:
    if '' == line.strip():
      for field in USEFUL_FIELDS:
        if field not in d:
          d[field] = 'NA'
      return(d)

    l = line.rstrip()
    fields = l.split(': ')
    if fields[0] in USEFUL_FIELDS:
      # store values with same key (eg. memberUid) in an array
      if fields[0] in d:
        if isinstance(d[fields[0]], list):
          d[fields[0]].append(fields[1])
        else:
          d[fields[0]] = [d[fields[0]], fields[1]]
      else:
        d[fields[0]] = fields[1]

  for field in USEFUL_FIELDS:
    if field not in d:
      d[field] = 'NA'
  return(d)


def read_fd_until_empty_line(fd):
  for line in fd:
    if '' == line.strip(): return


if __name__ == '__main__':
  main(sys.argv)

# ###
#real    1m57.750s
#user    1m54.931s
#sys     0m4.790s

# dev measures
#real    1m11.059s
#user    1m6.728s
#sys     0m3.536s

