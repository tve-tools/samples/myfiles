#!/bin/bash

fss="$@"

[ "x$fss" == "x" ] && echo 'fs not provided; stopping here...' && exit 0

for fs in $fss; do
  echo $fs
  umount $fs
  fsck -y $fs
  resize2fs $fs
  mount $fs
done


