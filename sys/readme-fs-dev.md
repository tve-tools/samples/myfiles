```bash
ansible dhadlx198,dhadlx200,dhadlx201,dhadlx202 -b -o -m script -a ./extend-fs.sh |./parse-ansible-output.py |tee 198-202.log

ansible Datanode -m shell -a 'df -h |grep /data|sort' |grep -v '3..G'
ansible "~dhadlx1[2][0-9]|dhadlx15[0-9]|dhadlx130" -l Datanode -m shell -a 'df -h |grep /data|sort' |grep -v '3..G'

ansible dhadlx414 -m shell -b -a 'pvs'

ansible dhadlx153 -b -o -m script -a './fix-fs.sh /dev/mapper/HaddVg-data4 /dev/mapper/HaddVg-data5' |./parse-ansible-output.py |tee fix-153.log
ansible Datanode -m copy -a "src=fix-fs.sh dest=/home/x195076 mode=0555"

```
