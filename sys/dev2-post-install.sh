#!/bin/bash

#usage: ansible --check dhadlx393 -b -m script -a ./dev-post-install.sh

## umount temp FS created in order to increase FS
umount /applis/data

lvremove -y /dev/HaddVg/data
vgremove -f /dev/HaddVg

## remove entry from fstab
sed -i '' -e 's:^.*/applis/data .*$::' /etc/fstab

## create VG
vgcreate HaddVg /dev/sdc

## create LV

fss='data1 data2 data3 data4 data5'

for fs in $fss; do
  echo $fs
  lvcreate --yes -L300g -n $fs HaddVg
  mkfs.ext4 /dev/HaddVg/$fs
  mkdir -p /$fs
  cp -p /etc/fstab ~/fstab.tve
  echo "/dev/HaddVg/$fs /$fs ext4 defaults 0 0" >>/etc/fstab
  mount /$fs
done

lvcreate --yes -L30g -n hdp HaddVg
mkfs.ext4 /dev/HaddVg/hdp
mkdir -p /usr/hdp
echo "/dev/HaddVg/hdp /usr/hdp ext4 defaults 0 0" >>/etc/fstab
mount /usr/hdp


## java install
yum update -y
yum install -y java-1.8.0-openjdk

sudo alternatives --install /etc/alternatives/java_sdk java_sdk $(dirname $(dirname $(readlink $(readlink $(which java))))) 1

sed -i '' -e 's:^# End of file.*$::' /etc/security/limits.conf

echo '* hard core 0
* - maxlogins 10
* soft nofile 32768
* hard nofile 32768
* soft nproc 65536
* hard nproc 65536
# End of file
' |tee -a /etc/security/limits.conf

## copy /etc/hosts
cp -p /etc/hosts /etc/hosts.init.bak
cp -p /etc/krb5.conf /etc/krb5.conf.init.bak
cp -p /etc/sssd/sssd.conf /etc/sssd/sssd.conf.init.bak
#scp -p  -i 
echo "Run ansible-playbook --check update-host.yml -e \"line='192.xxx.xxx.xxx $(hostname).haas.socgen $(hostname)'\""
echo "Connect to '$(hostname)' and run 'sudo scp -p x999999@dhadlx135:/etc/hosts /etc'"
echo "Run ansible-playbook --check -l $(hostname) add-repo.yml"
echo "from dhadlx135, run playbook myfiles/playbooks/update_ldap_conf.yml (cf. usage in plabook file)"
echo "Owing to issue with the role run by the previous playbook, connect to '$(hostname)' and run 'sudo scp -p x999999@dhadlx120:/etc/krb5.conf /etc'"
echo "Owing to issue with the role run by the previous playbook, connect to '$(hostname)' and edit /etc/sssd/sssd.conf as described below"
echo "Check sssd and kerberos is correctly set up by running 'id X999999'"
echo "copy truststore, keystore across... (TODO check socle_pki role)"

# scp -p x999999@dhadlx120.haas.socgen:/etc/pki/java/keystore.jks /etc/pki/java/keystore.jks
# chown hdfs:hadoop /etc/pki/java/keystore.jks; chmod 640 /etc/pki/java/keystore.jks

# scp -p x999999@dhadlx120.haas.socgen:/etc/pki/java/truststore.jks /etc/pki/java/truststore.jks
# chown hdfs:hadoop /etc/pki/java/truststore.jks; chmod 640 /etc/pki/java/truststore.jks


echo "(optional) On '$(hostname)', add ssh pub key to install user, cf x999999, authorized_keys / OR use your own private key when requested by Ambari"
echo "Add node in Ambari console"
echo "Once in the 'add host to Ambari' process and if ambari agent deploy from Ambari console fails with ssl error, edit '$(hostname):/etc/ambari-agent/conf/ambari-agent.ini' and add force_https_protocol=PROTOCOL_TLSv1_2 in [security] section"


## update /etc/sssd/sssd.conf as described below:
## update krb5_realm
#krb5_realm = DDHAD
#
## add the following after: ldap_default_bind_dn = cn=bindunix,ou=Services,dc=haas,dc=socgen
# ldap_tls_reqcert = allow
# ldap_id_use_start_tls = False
# ldap_tls_cacert = /etc/openldap/cacerts/ca.pem
#
## add the following after: ldap_default_authtok = bindunix
# ldap_schema = rfc2307bis
# ldap_group_nesting_level=5
# ldap_group_object_class = groupofnames

