-- Settings
-- SET tez.queue.name EMPTY;
SET mapreduce.job.queuename EMPTY;
SET output.compression.enabled true;
SET output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;
SET pig.maxCombinedSplitSize 128849018880;

-- Load the logs
raw_data = load '$log' using PigStorage();

-- Store the logs
STORE raw_data INTO '${log}_to_be_compressed' USING PigStorage();
