#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import sys

if len(sys.argv) < 2: print('Insufficient args ! bye ...'); exit(0)

arg1 = sys.argv[1]

with open(arg1, 'r') as json_file:
  print('evtTime - reqUser - action - access - result')
  for row in json_file:

    if not row.startswith('{'):
      print(row.rstrip())
      continue

    data = json.loads(row)
    print(data['evtTime'] + ' - ' + data['reqUser'] + ' - ' + data['action'] + ' - ' + data['access'] + ' - ' + str(data['result']))
    if data['action'] == 'read': continue
    if data['access'] == 'setPermission': continue

    print(json.dumps(data, sort_keys=True, indent=2, separators=(',', ': ')))
