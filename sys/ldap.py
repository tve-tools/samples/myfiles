#!/usr/bin/env /applis/hadd/miniconda3/bin/python
# -*- coding: utf-8 -*-

import sys
import subprocess
import tempfile
from os.path import expanduser
import pandas as pd
import time
import os

USEFUL_FIELDS = [ 'dn', 'cn', 'memberOf', 'memberUid', 'uid', 'sn', 'mail' ]

def main(argv):
#  slapcat_file = run_slapcat()
  slapcat_file = 'n2.ldif'
  print(slapcat_file)

  try:
#    sanity_check(slapcat_file)
#    gkibgroups(slapcat_file)
    bsc_mcg_elk_groups(slapcat_file)
  finally:
#    os.remove(slapcat_file)
    None


def gkibgroups(ldiffile):
  df = load_slapcat(ldiffile, ['dn', 'modifyTimestamp'])
#  df.to_csv('dump.csv', header=True, index=False)
  gkib_groups = df[df['dn'].str.contains('gkib')].sort_values('modifyTimestamp')
  gkib_groups.to_csv('gkib_groups.csv', header=True, index=False)

def bsc_mcg_elk_groups(ldiffile):
  df = load_slapcat(ldiffile, ['dn', 'member'])
  data = df[df['dn'].str.contains('gela_bsc_mcg|geladm_bsc_mcg|gkib_bsc_mcg|gela_bsc_a2827mcg|gkib_bsc_a2827mcg_hprd')]
  print(data.to_json())
  data.to_csv('bsc_mcg_elk_groups.csv', header=True, index=False)



def sanity_check(ldiffile):
  df = load_slapcat(ldiffile)
  df1 = get_user(df, '195076')
  df1.to_csv('sanity_check.csv', header=True, index=False)


def get_user(df, matricule):
  contain_values = df[df['uid'].str.contains(matricule)]
  required_cols = contain_values[['cn', 'sn', 'mail', 'uid']].sort_values('uid')
  required_cols.to_csv('users_props.csv',header=True,index=False)
  return(required_cols)


def run_slapcat():
  home = expanduser("~")
  prefix = '.' + os.path.splitext(os.path.basename(sys.argv[0]))[0] + '_'
  f = tempfile.NamedTemporaryFile(mode='w+b', delete=False, dir=home, prefix=prefix, suffix='.ldif')

  cmd = r'sudo slapcat -n2 -o ldif-wrap=no'

  try:
    subprocess.run(cmd, shell=True, timeout = 120, stdout = f)
  except subprocess.CalledProcessError as e:
    print(e.output)
  f.close()

  return(f.name)


def load_slapcat(filename, useful_fields = USEFUL_FIELDS):
  df = pd.DataFrame(columns = useful_fields)
  df_counter = 0
  with open(filename) as fd:
    for line in fd:
      # do not load kerberos entries (in the case kerberos is using ldap as backend)
      if line.startswith('dn: krb'):
        read_fd_until_empty_line(fd)

      else:
        a = transpose_lines(line, fd, useful_fields)
        df.loc[df_counter] = a
        df_counter += 1

  return(df)


def transpose_lines(line, fd,useful_fields):
  d = {}
  if line.rstrip().startswith('dn:'):
    d['dn'] = line.rstrip()[4:]
  else:
    print(f"{line} is not dn!")

  for line in fd:
    if '' == line.strip():
      for field in useful_fields:
        if field not in d:
          d[field] = 'NA'
      return(d)

    l = line.rstrip()
    fields = l.split(': ')
    if fields[0] in useful_fields:
      # store values with same key (eg. memberUid) in an array
      if fields[0] in d:
        if isinstance(d[fields[0]], list):
          d[fields[0]].append(fields[1])
        else:
          d[fields[0]] = [d[fields[0]], fields[1]]
      else:
        d[fields[0]] = fields[1]

  for field in useful_fields:
    if field not in d:
      d[field] = 'NA'
  return(d)


def read_fd_until_empty_line(fd):
  for line in fd:
    if '' == line.strip(): return


if __name__ == '__main__':
  main(sys.argv)

# ###

