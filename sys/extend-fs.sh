#!/bin/bash

# usage: ansible Datanode -m script -b -o -a ./extend-fs.sh -l dhadlx127,dhadlx128 |./parse-ansible-output.py

COMMIT=Y

typeset -i avail_space
avail_space=$(pvs |grep "/dev/sdb" |tr -s " " |cut -d " " -f 7- |sed -e "s:....$::")
avail_space_str=$(pvs |grep "/dev/sdb" |tr -s " " |cut -d " " -f 7-)

typeset -i fs_count
fs_count=$(df -h |grep '/data' |wc -l)

#echo "$avail_space_str / $fs_count"

typeset -i fs_inc
fs_inc=$(echo "( $avail_space * 1024 - 20480 ) / $fs_count" |bc)

[ $fs_inc -lt 5000 ] && echo "Too low fs increment: $fs_inc; not performing fs increase..." && exit 0

for fs in $(df -h |grep '/data' |cut -d ' ' -f 1 |sort); do
  cmd="lvextend -L +${fs_inc}M $fs"
  echo $cmd
  [ "$COMMIT" == 'Y' ] && eval $cmd
  cmd="resize2fs $fs"
  echo $cmd
  [ "$COMMIT" == 'Y' ] && eval $cmd
done

