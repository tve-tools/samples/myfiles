#!/bin/bash

export ELK_BASE=/applis/hadi/elk

#alias start-es-node1='export ES_PATH_CONF=$ELK_BASE/node1/config && export ES_HEAP_SIZE=2g && export MAX_LOCKED_MEMORY=unlimited  && nohup $ELK_BASE/produits/elasticsearch-current/bin/elasticsearch -p $ELK_BASE/node1/node1.pid >$ELK_BASE/logs/node1.out 2>&1 &'
#alias start-es-node2='export ES_PATH_CONF=$ELK_BASE/node2/config && export ES_HEAP_SIZE=512m && export MAX_LOCKED_MEMORY=unlimited  && nohup  $ELK_BASE/produits/elasticsearch-current/bin/elasticsearch -p $ELK_BASE/node2/node2.pid  >$ELK_BASE/logs/node2.out 2>&1 &'
#alias start-es-node3='export ES_PATH_CONF=$ELK_BASE/node3/config && export ES_HEAP_SIZE=4g && export MAX_LOCKED_MEMORY=unlimited  && nohup $ELK_BASE/produits/elasticsearch-current/bin/elasticsearch -p $ELK_BASE/node3/node3.pid  >$ELK_BASE/logs/node3.out 2>&1 &'
#alias stop-es-node1='test -f $ELK_BASE/node1/node1.pid && kill $(cat $ELK_BASE/node1/node1.pid) || echo "no pid found"'
#alias stop-es-node2='test -f $ELK_BASE/node2/node2.pid && kill $(cat $ELK_BASE/node2/node2.pid) || echo "no pid found"'
#alias stop-es-node3='test -f $ELK_BASE/node3/node3.pid && kill $(cat $ELK_BASE/node3/node3.pid) || echo "no pid found"'
#alias stop_kibana='kill $(ps -fe |grep -v grep |grep kibana |tr -s " "|cut -d " " -f 2)'
#alias start-kibana='nohup $ELK_BASE/produits/kibana-current/bin/kibana -c $ELK_BASE/kibana/conf/kibana.yml  -p $ELK_BASE/kibana/kibana.pid  >$ELK_BASE/logs/kibana.out 2>&1 &'

stop_node() {
  node=$1
  test -f $ELK_BASE/${node}/${node}.pid && kill $(cat $ELK_BASE/${node}/${node}.pid) || echo "no pid found"
}

start_node() {
  node=$1
  mem=$2
  export ES_PATH_CONF=$ELK_BASE/${node}/config
  export ES_HEAP_SIZE=${mem}
  export MAX_LOCKED_MEMORY=unlimited  
  nohup $ELK_BASE/produits/elasticsearch-current/bin/elasticsearch -p $ELK_BASE/${node}/${node}.pid  >$ELK_BASE/logs/${node}.out 2>&1 &

}

stop_kibana() {
  kill $(ps -fe |grep -v grep |grep kibana |tr -s " "|cut -d " " -f 2)
}

start_kibana() {
  nohup $ELK_BASE/produits/kibana-current/bin/kibana -c $ELK_BASE/kibana/conf/kibana.yml  -p $ELK_BASE/kibana/kibana.pid  >$ELK_BASE/logs/kibana.out 2>&1 &

}



set -x
stop_kibana
stop_node node3
stop_node node2
stop_node node1

sleep 10

start_node node1 2g
sleep 2
start_node node2 512m
sleep 2
start_node node3 4g
sleep 2
start_kibana
set +x


