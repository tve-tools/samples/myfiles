#!/usr/bin/env python
# usage: ansible Datanode -m script -b -o -a ./extend-fs.sh -l dhadlx127,dhadlx128 |./parse-ansible-output.py
# usage: ansible Datanode -m script -b -o -a ./extend-fs.sh |./parse-ansible-output.py
import sys
import json

# use stdin if it's full                            
if not sys.stdin.isatty():
  input_stream = sys.stdin
# otherwise, read the given filename                      
else:
  try:
    input_filename = sys.argv[1]
  except IndexError:
    message = 'need filename as first argument if stdin is not full'
    raise IndexError(message)
  else:
    input_stream = open(input_filename, 'r')

for line in input_stream:
  a = line.strip().split('|')
  host = a[0].strip()
  a = a[1].strip().split('=>')
  status = a[0].strip()
  j = json.loads(a[1].strip())
  print('  ** ' + host + ' ** ')
  print('\n'.join(j['stdout_lines']))


#dhadlx198 | CHANGED => {"changed": true, "rc": 0, "stderr": "Shared connection to dhadlx198.dns21.socgen closed.\r\n", "stderr_lines": ["Shared connection to dhadlx198.dns21.socgen closed."], "stdout": "343.00g / 5\r\n", "stdout_lines": ["343.00g / 5"]}
