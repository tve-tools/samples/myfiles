#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import shlex
import sys
import json
import time


def runcmd(cmdstr):
  if cmdstr.strip() == '':
    raise Exception('no command provided !')

  result = subprocess.run(shlex.split(cmdstr), stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
#  print('stderr: ' + result.stderr.strip())
#  print('stdout: ' + result.stdout.strip())

  if result.returncode != 0:
    print(cmdstr)
    print('stderr: ' + result.stderr.strip())
    print('stdout: ' + result.stdout.strip())
    raise Exception('command generated error !')
  else:
    return(result.stdout.strip())


def run_healthcheck(url, node_total):
  output = runcmd(url)
  for line in json.loads(output):
    print(line)
    if line['status'] != 'green' or int(line['node.total']) != node_total:
      return False

  return True


#def restart_nodes(jsonfilename, healthcheck_url, sleep_duration = 300):
def restart_nodes(conf_filename):
  conf = load_conf(conf_filename)
  json_filename = conf['elk_nodes']
  healthcheck_url = conf['healthcheck_url']
  sleep_duration = conf['initial_sleep']
  step_sleep = conf['step_sleep']
  node_total = conf['node.total']
  check_option = ' --check '
  if 'run' in conf and conf['run'] == True:
    check_option = ''
  else:
    sleep_duration = 1
    step_sleep = 1

  with open(json_filename) as f:
    for node in json.load(f):
      (host, logicalnode) = node['name'].split('-')
      cmd = 'ansible ' + host + check_option + " -b -o -m shell -a 'systemctl restart elasticsearch-" + logicalnode + "'"
      print('\n\n  -------\n ' + cmd)
      print(runcmd(cmd))
      print(node['name'] + ' - sleeping for: ' + str(sleep_duration) + ' sec(s)')
      time.sleep(sleep_duration)
      while True:
        if run_healthcheck(healthcheck_url, node_total): break
        print(node['name'] + ' sleeping for: ' + str(step_sleep) + ' sec(s)')
        time.sleep(step_sleep)


def load_conf(conf_filename):
  with open(conf_filename) as f:
    return json.load(f)
#    for key in conf:
#      print(conf[key])

#load_conf(sys.argv[1])


#restart_nodes(sys.argv[1], sys.argv[2], int(sys.argv[3]))
restart_nodes(sys.argv[1])


# usage: run ansible-cfg
# usage: run GET _cat/nodes?v&h=name&s=name&format=json to get elk node list
# usgae: edit elk-nodes-dev-nominal-restart.json and update healthcheck_url, elk_nodes, node.total
# usage: IN ORDER to prevent the script from failing due to searchnode being restarted and healthcheck_url being unavailable, restart the searchnode first...
# usage: myfiles/sys/restart-elk-stack.py elk-nodes-dev-nominal-restart.json

# usage: myfiles/sys/restart-elk-stack.py myfiles/sys/restart-elk-stack.json
