#!/bin/python

# checking if leader or not:
# $ echo mntr |nc phadlx50.haas.socgen 2181

import sys
import subprocess

zknode = 'phadlx51.haas.socgen:2181'

def main(argv):
  getznodecount('/rmstore', 8)
#  getznodecount('/hbase-secure', 4)


def getznodecount(branch, level=0):
  if level <= 0:
    return
#  else:
#    print('recursion level: ' + str(level))

  zkcmd = '/usr/hdp/current/zookeeper-client/bin/zkCli.sh -server ' + zknode + ' get "' + branch + '"'
#  zkcmd = '/usr/hdp/current/zookeeper-client/bin/zkCli.sh -server  phadlx50.haas.socgen:2181 get "' + branch + '"'
  arr, stde = runcmd(zkcmd)
  childCount = '0'
  for line in stde:
    if 'numChildren' in line:
      childCount = line.split()[2]
      break

  print(branch + ' ' + childCount)
  if childCount != '0':
    # exclude table znode
#    if not branch.endswith('table') and not branch.endswith('table-lock') and not branch.endswith('RMAppRoot') and not branch.endswith('RMDelegationTokensRoot') and not branch.endswith('RMDTMasterKeysRoot'):
    processchildren(branch, level)


def processchildren(branch, level):
  zkcmd = '/usr/hdp/current/zookeeper-client/bin/zkCli.sh -server ' + zknode + ' ls "' + branch + '"'
  arr, stde = runcmd(zkcmd)
  children = ''
  for line in arr:
    if line.startswith('['):
      children = line
      break

  children = children.strip('[').strip(']').replace(',', '').split()

  childcount = 0
  for child in children:
    childcount += 1
    if len(children) < 20:
      getznodecount(branch + '/' + child, level - 1)
    else:
      if childcount < 5:
        getznodecount(branch + '/' + child, level - 1)
      else:
        break


def runcmd(cmd):
  process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
  output, error = process.communicate()
  return(output.splitlines(), error.splitlines())


if __name__ == '__main__':
  main(sys.argv)


