#!/usr/bin/env /usr/bin/python

import sys
import yaml
import json
from jinja2 import Template
import getpass
import requests

class RoleMigrate:
  def __init__(self, filename):
    self.role_shield_filename = filename
    self.kib_index_map = {}

    self.ELADM_PRIV = [ "feature_discover.all", "feature_visualize.all", "feature_dashboard.all", "feature_dev_tools.all", 
      "feature_advancedSettings.read", "feature_indexPatterns.read", "feature_savedObjectsManagement.all" ]


    self.user = 'X195076'
    self.passwd = 'Qqt4s1tObLrxvOKr_Z8W'


  def get_mapping(self):
    print('get_mapping')
    with open(self.role_shield_filename) as f:
      yaml_data = yaml.safe_load(f)

    for rolename in yaml_data:
      if rolename.startswith('kib_') or rolename.startswith('kibh_'):
        role_specs = yaml_data[rolename]
        for index in role_specs['indices']:
          if 'kibana' in str(index['names']):
            b = rolename.split('_')
            pattern_a = b[1] + '_' + b[2]
            self.kib_index_map[pattern_a] = index['names']


  def load_elk_role_file(self):
    with open(self.role_shield_filename) as f:
      yaml_data = yaml.safe_load(f)

    for rolename in yaml_data:
      role_specs = yaml_data[rolename]

      # skip 'eladm_kibana' role
      if rolename == 'eladm_kibana': continue

      if rolename.startswith('eladm_'):
        b = rolename.split('_')
        try:
          pattern_a = b[1] + '_' + b[2]
        except:
          print('Error when processing: ' + rolename)
          raise

        try:
          kibana_index = self.kib_index_map[pattern_a]
        except:
          print(pattern_a + ' not found !!!')
#          raise
          kibana_index = '.toto'


        if 'applications' in role_specs:
          role_specs['applications'].append(self.generate_role_app_spec('kibana-' + kibana_index, self.ELADM_PRIV))
        else:
          role_specs['applications'] = [ self.generate_role_app_spec('kibana-' + kibana_index, self.ELADM_PRIV) ]
        self.sample_role_specs = role_specs

  
  def generate_role_app_spec(self, app, privs = None):
    privs = privs or self.ELADM_PRIV

    role_app_spec = {}
    role_app_spec['application'] = app 
    role_app_spec['privileges'] = privs
    role_app_spec['resources'] = [ "space:default" ]


    return role_app_spec
    

  def create_update_role(self, role, payload):
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    url = 'https://dhadlx291.haas.socgen:9200/_security/role/' + role
    print(json.dumps(self.sample_role_specs))
    response = requests.put(url, auth = (self.user, self.passwd), data = json.dumps(self.sample_role_specs), headers = headers)
    print(response.json())


if __name__ == '__main__':
  role_migrate = RoleMigrate(sys.argv[1])
  role_migrate.get_mapping()

  role_migrate.load_elk_role_file()
  role_migrate.create_update_role('eladm_bsc_mcg', '')

