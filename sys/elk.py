#!/bin/python

import sys
 
file = sys.argv[1]

def main(argv):
  process_index_file(file)
 
def process_index_file(file, header=True):
  try:
    inf = open(file)
  except IOError:
    print("error opening file '" + file + "'")
    return

  for line in inf:
    arr = line.strip().split()
    if header:
      header = False
      print('\t'.join(arr))
      continue

    a = arr[2][5:].split('.')
    arr[2] = a[2] + '/' + a[1] + '/' + a[0]
    arr[8] = convert_to_unit(arr[8])
    arr[9] = convert_to_unit(arr[9])
    print('\t'.join(arr))
  inf.close()

def convert_to_unit(istr):
  istr = istr[:-1]
  prefix = istr[-1]
  if prefix.isdigit():
    return(istr)
  if prefix == 'k': prefix = 1024
  if prefix == 'm': prefix = 1024 * 1024
  if prefix == 'g': prefix = 1024 * 1024 * 1024
  if prefix == 't': prefix = 1024 * 1024 * 1024 * 1024
  if prefix == 'p': prefix = 1024 * 1024 * 1024 * 1024 * 1024
  return(str(int(float(istr[:-1]) * prefix)))
  

if __name__ == '__main__':
  main(sys.argv)

