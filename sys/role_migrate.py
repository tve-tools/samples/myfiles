#!/usr/bin/env /usr/bin/python
# -*- coding: utf-8 -*-

import sys
import yaml
import json
import getpass
import requests

class RoleMigrate:
  def __init__(self, conf_file):
    with open(conf_file) as f:
        self.conf = yaml.safe_load(f)

    self.kib_index_map = {}

    if 'ela_admin' not in self.conf:
      self.user = raw_input('elastic admin  login: ')
    else:
      self.user = self.conf['ela_admin']

    if 'ela_passwd' not in self.conf:
      self.passwd = getpass.getpass('elastic admin passwd: ')
    else:
      self.passwd = self.conf['ela_passwd']


  def get_mapping(self):
    with open(self.conf['ela_role_file']) as f:
      yaml_data = yaml.safe_load(f)

    for rolename in yaml_data:
      if rolename.startswith('kib_') or rolename.startswith('kibh_'):
        role_specs = yaml_data[rolename]
        for index in role_specs['indices']:
          if 'kibana' in str(index['names']):
            b = rolename.split('_')
            pattern_a = b[1] + '_' + b[2]
            self.kib_index_map[pattern_a] = index['names']


  def dump_mapping(self):
    if self.kib_index_map is None: 
      self.get_mapping()
    print(self.kib_index_map)


  def migrate_role_file(self):
    print(' >> migrate_role_file...')
    with open(self.conf['ela_role_file']) as f:
      yaml_data = yaml.safe_load(f)

    for rolename in yaml_data:
      role_specs = yaml_data[rolename]

      if 'filter' in self.conf and self.conf['filter'] not in rolename:
        print(rolename + ' does not contain filter ' + self.conf['filter'] + ', skipping...')
        continue

      print(rolename)

      # skip 'eladm_kibana' role
      if rolename == 'eladm_kibana': continue

      if rolename.startswith('eladm_') or rolename.startswith('kib_') :
        application_spec = self.gen_role_app_spec(rolename)
        if not application_spec:
          continue

        if 'applications' in role_specs:
          role_specs['applications'].append(application_spec)
        else:
          role_specs['applications'] = [ application_spec ]

      if 'debug' in self.conf and not self.conf['debug']:
#        role_migrate.create_update_role(rolename, role_specs)
        role_migrate.call_api('/_security/role/', rolename, role_specs)
      else:
        print(json.dumps(role_specs))
        print('')
#        print(json.dumps(role_specs, indent=1))

  
  def migrate_role_mapping(self):
    print(' >> migrate_role_mapping...')
    with open(self.conf['ela_role_mapping']) as f:
      yaml_data = yaml.safe_load(f)

    for rolename in yaml_data:
      mapping_specs = yaml_data[rolename]

      if 'filter' in self.conf and self.conf['filter'] not in rolename:
        print(rolename + ' does not contain filter ' + self.conf['filter'] + ', skipping...')
        continue

      print(rolename)

      # skip 'eladm_kibana' role
      if rolename == 'eladm_kibana': continue

      payload = self.gen_mapping_payload(rolename, mapping_specs, self.conf['role_mapping_struct'])

      if 'debug' in self.conf and not self.conf['debug']:
        role_migrate.call_api('/_security/role_mapping/', rolename, payload)
      else:
        print(json.dumps(payload))
        print('')

  
  def gen_mapping_payload(self, role, specs, template):
    template['roles'] = role
    template['rules']['all'][0]['field']['groups'] = specs
    return(template)


  def gen_role_app_spec(self, rolename):
    b = rolename.split('_')
    try:
      pattern_a = b[1] + '_' + b[2]
    except:
      print('Error when trying to extract pattern from rolename: ' + rolename + '. Skipping this rolename for migration...')
      return None

    try:
      kibana_index = self.kib_index_map[pattern_a]
    except:
      print('Cannot find kibana_index for: ' + pattern_a + '. Skipping rolename: ' + rolename + ' for migration...')
      return None

    if rolename.startswith('eladm_'):
      privs = self.conf['eladm_priv']
    elif rolename.startswith('kib_'):
      privs = self.conf['kib_priv']
    else:
      print('unimplemented ...')
      return None

    role_app_spec = {}
    role_app_spec['application'] = self.conf['kib_app_prefix'] + kibana_index
    role_app_spec['privileges'] = privs
    role_app_spec['resources'] = [ "space:default" ]
    return role_app_spec
    

  def call_api(self, api_endpoint, role, payload):
    print('call_api ' + api_endpoint + role + ' ' + json.dumps(payload))
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    url = self.conf['ela_endpoint'] + api_endpoint + role
    response = requests.put(url, auth = (self.user, self.passwd), data = json.dumps(payload), headers = headers)
    print('call_api response: ' + str(response.status_code) + ' - ' + response.text)


if __name__ == '__main__':
  role_migrate = RoleMigrate(sys.argv[1])

  if role_migrate.conf['migrate'].lower() in [ 'role', 'both' ]:
    role_migrate.get_mapping()
#    role_migrate.dump_mapping()
    role_migrate.migrate_role_file()

  if role_migrate.conf['migrate'].lower() in [ 'mapping', 'both' ]:
    role_migrate.migrate_role_mapping()

