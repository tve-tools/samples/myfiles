#!/bin/bash
echo "`date`: `pwd`" > /tmp/output
#SCRIPTNAME=$(basename ${0%.*}); exec >>./$SCRIPTNAME.log 2>&1
SCRIPTNAME=$(basename ${0%.*}); exec >/tmp/$SCRIPTNAME.log 2>&1

set -x
echo "$HADOOP_TOKEN_FILE_LOCATION"

hdfs dfs -get oozie-compressor/compressor_ranger.pig
#hdfs dfs -get oozie-compressor/dtvcompress.keytab
#hdfs dfs -get oozie-compressor/pig-krb.conf
#export KRB5CCNAME=/tmp/.ranger-compress-ksession
#kinit -kt dtvcompress.keytab dtvcompress/admin
#rm -f dtvcompress.keytab
#KRB5CCNAME=/tmp/.ranger-compress-ksession hdfs fetchdt --renewer hdfs ./delegation.token

pwd
ls -ltra
#klist

LOG_TO_COMPRESS='hdfs://hdtvcluster01:8020/user/dtvcompress/sample-big-file.txt'
#pig -P pig-krb.conf -f compressor_ranger.pig -param log=$LOG_TO_COMPRESS
pig -Dmapreduce.job.credentials.binary=$HADOOP_TOKEN_FILE_LOCATION -Dtez.credentials.path=$HADOOP_TOKEN_FILE_LOCATION -f compressor_ranger.pig -param log=$LOG_TO_COMPRESS
set +x

#usage: oozie job -oozie https://hdtvf01.dns21.socgen:11443/oozie -config job.properties -run

