#!/bin/bash

hdfs dfs -put -f ../oozie-compressor
OOZIE_ID=$(oozie job -oozie https://hdtvf01.dns21.socgen:11443/oozie -config job.properties -run |sed -e 's/^job: //')
yarn application -list -appStates ALL |grep $OOZIE_ID
echo 'yarn logs -applicationId application_***'

