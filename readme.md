## 'Interesting' files
* [ELK habilitation migration from csv to ELK roles](sys/role_migrate.py)
* [Python pandas usage](sys/process_ldap.py)
* [Kafka MirrorMaker](kafka/mirror-maker-conf)

