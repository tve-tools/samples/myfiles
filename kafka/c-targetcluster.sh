#!/bin/bash

# usage: brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen' ./c.sh totp plain
# usage: export brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen'
# usage: ./c.sh totp plain

topic=$1
listener=$2
kspass=$3
brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen'

[ "x$topic" == "x" ] && echo "topic not provided..." && exit 0
[ "x$listener" == "x" ] && echo "listener not provided..." && exit 0
[ "x$brokerlist" == "x" ] && echo "brokerlist not set..." && exit 0
[ "x$kspass" == "x" ] && kspass=iFUKtnhYGZ


fixbrokerlist() {
  port=$1
  mybrokerlist=''
  for broker in ${brokerlist//,/ }; do
    if [ "x$mybrokerlist" == "x" ] ; then
      mybrokerlist="$broker:$port"
    else
      mybrokerlist="$mybrokerlist,$broker:$port"
    fi
  done
  brokerlist=$mybrokerlist
}

ssl() {
  fixbrokerlist 6667
  cmd="kafka-console-consumer.sh --bootstrap-server $brokerlist --topic $topic --new-consumer --from-beginning --security-protocol SSL --consumer-property ssl.truststore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --consumer-property ssl.truststore.password=$kspass --consumer-property ssl.keystore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --consumer-property ssl.keystore.password=$kspass"

  eval "$cmd"
  echo "$cmd"
}


case "$listener" in
  ssl)
    ssl
    ;;
  *)
    echo "invalid listener: $listener"
    exit 0
esac

