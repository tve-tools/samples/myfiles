#!/bin/bash


#kinit 
# or kinit X195076 (or whatever available principal allowed on topic002)

KAFKA_OPTS="-Djava.security.auth.login.config=./kafkaclient.jaas -Dlog4j.configuration=file:./log4j.properties" kafka-console-consumer.sh --bootstrap-server dbebpprdk8slx5821.fr.world.socgen:9093,dbebpprdk8slx5822.fr.world.socgen:9093,dbebpprdk8slx5823.fr.world.socgen:9093 --topic topic002 --new-consumer --from-beginning --security-protocol SASL_PLAINTEXT

# plaintext (use plaintext listener), un-authenticated client (anonymous acl needs to be applied on topic first)
KAFKA_OPTS="-Djava.security.auth.login.config=./kafkaclient.jaas -Dlog4j.configuration=file:./log4j.properties" kafka-console-consumer.sh --bootstrap-server dbebpprdk8slx5821.fr.world.socgen:9095,dbebpprdk8slx5822.fr.world.socgen:9095,dbebpprdk8slx5823.fr.world.socgen:9095 --topic topic002 --new-consumer --from-beginning
