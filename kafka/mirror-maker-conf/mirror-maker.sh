#export KAFKA_KERBEROS_PARAMS="-Djava.security.auth.login.config=/applis/hadd/produits/MirrorMakerConf/MirrorMakerConf/kafka_jaas.conf"
#export KAFKA_CLIENT_KERBEROS_PARAMS="-Djava.security.auth.login.config=/applis/hadd/produits/MirrorMakerConf/MirrorMakerConf/kafka_jaas.conf"
# because /tmp is cleaned every day
#kinit -kt /applis/hadd/produits/MirrorMakerConf/mcgpvadm.applicatif.keytab mcgpvadm/dhadlx30.haas.socgen

KAFKA_OPTS='-Djava.security.auth.login.config=/home/X195076/myfiles/kafka/mirror-maker-conf/kafkaclient.jaas -Dlog4j.configuration=file:/home/X195076/myfiles/kafka/mirror-maker-conf/log4j.properties'

TOPIC_NAME='topic-ssl-tve04-91'
/usr/hdp/current/kafka-broker/bin/kafka-run-class.sh kafka.tools.MirrorMaker  --new.consumer  --consumer.config /applis/hadd/produits/mirror-maker-conf/consumer-sourcecluster-ssl.properties --num.streams 1 --producer.config /applis/hadd/produits/mirror-maker-conf/producer-targetcluster-sasl.properties --whitelist=$TOPIC_NAME --abort.on.send.fail true

#nohup /usr/hdp/current/kafka-broker/bin/kafka-run-class.sh kafka.tools.MirrorMaker  --new.consumer  --consumer.config /applis/hadd/produits/MirrorMakerConf/consumer_sourceCluster1_ssl.properties --num.streams 1 --producer.config /applis/hadd/produits/MirrorMakerConf/producer_targetCluster2_sasl.properties  --whitelist=$TOPIC_NAME --abort.on.send.fail true &

