#!/bin/bash

#kinit 
# or kinit X195076 (or whatever available principal allowed on topic002)

KAFKA_OPTS="-Djava.security.auth.login.config=./kafkaclient.jaas -Dlog4j.configuration=file:./log4j.properties" kafka-console-producer.sh --broker-list dbebpprdk8slx5821.fr.world.socgen:9093,dbebpprdk8slx5822.fr.world.socgen:9093,dbebpprdk8slx5823.fr.world.socgen:9093 --topic topic002 --security-protocol SASL_PLAINTEXT

# plaintext (use plaintext listener), un-authenticated client (anonymous acl needs to be applied on topic first)
KAFKA_OPTS="-Djava.security.auth.login.config=./kafkaclient.jaas -Dlog4j.configuration=file:./log4j.properties" kafka-console-producer.sh --broker-list dbebpprdk8slx5821.fr.world.socgen:9095,dbebpprdk8slx5822.fr.world.socgen:9095,dbebpprdk8slx5823.fr.world.socgen:9095 --topic topic002

# ssl
# /usr/hdp/current/kafka-broker/bin/kafka-console-producer.sh

kafka-console-producer.sh --broker-list dbebpprdk8slx5821.fr.world.socgen:6667,dbebpprdk8slx5822.fr.world.socgen:6667,dbebpprdk8slx5823.fr.world.socgen:6667 --topic topic002 --security-protocol SSL --producer-property ssl.truststore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --producer-property ssl.truststore.password=iFUKtnhYGZ --producer-property ssl.keystore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --producer-property ssl.keystore.password=iFUKtnhYGZ
