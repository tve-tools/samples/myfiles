#!/bin/bash

ZK=$1

[ "x$ZK" == "x" ] && echo 'please provide zk' && exit 0

#CLUSTER=$(kinit </dev/null 2>/dev/null |sed -e 's:.*@::' -e 's:\..*$::' |tr '[:upper:]' '[:lower:]')
CLUSTER='tve04'

TOPIC_SUFFIX="-$CLUSTER"
[ "x$2" != "x" ] && TOPIC_SUFFIX="$TOPIC_SUFFIX-$2"

## plaintext AND anonymous - listener PLAINTEXT:..:9095
#TOPIC=topic-plaintext-anon$TOPIC_SUFFIX
#sudo /usr/hdp/current/kafka-broker/bin/kafka-topics.sh --zookeeper $ZK:2181 --create --topic $TOPIC --partitions 2 --replication 2
#
#sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=$ZK:2181 --add --allow-principal user:ANONYMOUS --operation All --topic $TOPIC
#
#
## plaintext sasl - listener PLAINTEXTSASL:..:9093
#TOPIC=topic-plaintext-sasl$TOPIC_SUFFIX
#sudo /usr/hdp/current/kafka-broker/bin/kafka-topics.sh --zookeeper $ZK:2181 --create --topic $TOPIC --partitions 2 --replication 2
#
#sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=$ZK:2181 --add --allow-principal user:kafkasasl01 --operation All --topic $TOPIC

# ssl - listener SSL:..:6667
TOPIC=topic-ssl$TOPIC_SUFFIX
sudo /usr/hdp/current/kafka-broker/bin/kafka-topics.sh --zookeeper $ZK:2181 --create --topic $TOPIC --partitions 2 --replication 2

#sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties  zookeeper.connect=$ZK:2181 --add --allow-principal User:CN=prj01,OU=RESG,O=GROUPE_SOCIETE_GENERALE,L=Paris,ST=Ile_de_France,C=FR --operation write --operation read --operation describe --topic $TOPIC
sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=$ZK:2181 --add --allow-principal user:kafkasasl01 --operation All --topic $TOPIC
