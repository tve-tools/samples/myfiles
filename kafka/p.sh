#!/bin/bash

# usage: brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen' ./p.sh totp plain
# usage: export brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen'
# usage: export brokerlist=dbebpprdk8slx6650,dbebpprdk8slx6651,dbebpprdk8slx6652
# usage: ./p.sh totp plain

topic=$1
listener=$2
#brokerlist='dbebpprdk8slx5821.fr.world.socgen,dbebpprdk8slx5822.fr.world.socgen,dbebpprdk8slx5823.fr.world.socgen'
kspass=$3



[ "x$topic" == "x" ] && echo "topic not provided..." && exit 0
[ "x$listener" == "x" ] && echo "listener not provided..." && exit 0
#[ "x$msg" == "x" ] && echo "msg not provided..." && exit 0
[ "x$brokerlist" == "x" ] && echo "brokerlist not set..." && exit 0
[ "x$kspass" == "x" ] && kspass=iFUKtnhYGZ


fixbrokerlist() {
  port=$1
  mybrokerlist=''
  for broker in ${brokerlist//,/ }; do
    if [ "x$mybrokerlist" == "x" ] ; then
      mybrokerlist="$broker:$port"
    else
      mybrokerlist="$mybrokerlist,$broker:$port"
    fi
  done
  brokerlist=$mybrokerlist
}

sasl() {
  fixbrokerlist 9093
  eval "KAFKA_OPTS='-Djava.security.auth.login.config=/home/X195076/myfiles/kafka/kafkaclient.jaas -Dlog4j.configuration=file:/home/X195076/myfiles/kafka/log4j.properties' kafka-console-producer.sh --broker-list $brokerlist --topic $topic --security-protocol SASL_PLAINTEXT"
}

plain() {
  fixbrokerlist 9095
  eval "KAFKA_OPTS='-Djava.security.auth.login.config=/home/X195076/myfiles/kafka/kafkaclient.jaas -Dlog4j.configuration=file:/home/X195076/myfiles/kafka/log4j.properties' kafka-console-producer.sh --broker-list $brokerlist --topic $topic"
}

ssl() {
  fixbrokerlist 6667
  eval "kafka-console-producer.sh --broker-list $brokerlist --topic $topic --security-protocol SSL --producer-property ssl.truststore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --producer-property ssl.truststore.password=$kspass --producer-property ssl.keystore.location=/home/X195076/prj01/kafka.prj01.keystore.jks --producer-property ssl.keystore.password=$kspass"
}

case "$listener" in
  plain)
    plain
    ;;
  sasl)
    sasl
    ;;
  ssl)
    ssl
    ;;
  *)
    echo "invalid listener: $listener"
    exit 0
esac

