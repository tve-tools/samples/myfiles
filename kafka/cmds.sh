
### create ssl certificate

# (run on CA host: dbebpprdk8slx5834, dbebpprdk8slx6657)
mkdir ~/prj01
cd ~/prj01
keytool -genkey -alias prj01 -keystore kafka.prj01.keystore.jks -keyalg RSA -keysize 2048 -validity 3650 -ext san=dns:$(hostname -f)
keytool -certreq -alias prj01 -keystore kafka.prj01.keystore.jks -file prj01-kafka.csr
sudo openssl x509 -req -in prj01-kafka.csr -CA /etc/pki/tls/certs/$(hostname -s).crt -CAkey /etc/pki/tls/private/$(hostname -s).key -CAcreateserial -out prj01-kafka.crt -days 1825 -sha256
keytool -importcert -keystore kafka.prj01.keystore.jks -file /etc/pki/tls/certs/$(hostname -s).crt -alias my_ca
keytool -importcert -trustcacerts -keystore kafka.prj01.keystore.jks -file prj01-kafka.crt -alias prj01


### create sasl user
ssh dbebpprdk8slx5834 "sudo kadmin.local -q 'addprinc kafkasasl01'"

### extract from phadlx40
# /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=phadlx50.haas.socgen:2181,phadlx52.haas.socgen:2181,phadlx51.haas.socgen:2181/kafka --list --topic $opt_topic

Current ACLs for resource `Topic:gtstfohlsprodpvhaaccess`:
  User:ANONYMOUS has Allow permission for operations: Describe from hosts: *
  User:ANONYMOUS has Allow permission for operations: Read from hosts: *
  User:CN=hlsstream.haas.socgen,OU=RESG,O=GROUPE_SOCIETE_GENERALE,ST=Essonne,C=FR has Allow permission for operations: Describe from hosts: *
  User:CN=hlsstream.haas.socgen,OU=RESG,O=GROUPE_SOCIETE_GENERALE,ST=Essonne,C=FR has Allow permission for operations: Write from hosts: *

# /usr/hdp/current/kafka-broker/bin/kafka-topics.sh --zookeeper "phadlx50.haas.socgen:2181,phadlx52.haas.socgen:2181,phadlx51.haas.socgen:2181/kafka" --list |tee kafka-topics.list
__consumer_offsets
ambari_kafka_service_check
ambari_test_topic
assuybrprodevents

