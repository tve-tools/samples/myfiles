#!/bin/bash

[ ! -f ./kafkaclient.jaas ] && echo 'KafkaClient {
      com.sun.security.auth.module.Krb5LoginModule required
      useTicketCache=true
      serviceName="kafka";
};' |tee ./kafkaclient.jaas

[ ! -f ./log4j.properties ] && echo 'log4j.rootLogger=DEBUG, console, f

log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%d] %p %m (%c)%n
log4j.appender.console.Target=System.err
log4j.appender.console.threshold=WARN

log4j.appender.f=org.apache.log4j.FileAppender
log4j.appender.f.File=logs/log.out
log4j.appender.f.layout=org.apache.log4j.PatternLayout
log4j.appender.f.layout.ConversionPattern=[%d] %p %m (%c)%n
' |tee ./log4j.properties

kinit 
# or kinit X195076 (or whatever available principal allowed on topic002)

