#!/usr/bin/env python
# -*- coding: utf-8 -*-

# usage: ./get_kafka_conf.py --user X195076 --url http://127.0.0.1:8080
# usage:  ./get_kafka_conf.py --user X195076 --url https://phadlx198.haas.socgen:8443

from module_utils.ambari import AmbariApi
import json
import argparse
import time
import getpass
import sys
from urlparse import urlparse

global arguments
global password

def get_conf():
    u = urlparse(arguments["url"])
    ambariApi = AmbariApi(u.scheme, u.hostname, str(u.port), "basic")
    ambariApi.setCredentialsBasic(arguments["user"], password)

    result = ambariApi.getClusterName()
#    print(ambariApi)
#    print(result)
#    result = ambariApi.getClusterServices()
#    for item in result.json()["items"]:
#      print(item["ServiceInfo"]["service_name"])

    result = ambariApi.getCurrentServiceConf('KAFKA')
#    print(json.dumps(result.json(), indent=2))
    for configuration in result.json()["items"][0]["configurations"]:
      configuration_type = configuration["type"]
      for key in sorted(configuration["properties"].keys()):
        if key == 'content': continue
        print(configuration_type + " --- " + key + " --- " + configuration["properties"][key])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Testing the api', prog='test')
    parser.add_argument('--user', type=str, help='Specify the user account')
    parser.add_argument('--url', type=str, help='Specify Ambari url (e.g. http://127.0.0.1:8080)')
    arguments = vars(parser.parse_args())
    if(arguments["user"] is not None):
      password = getpass.getpass(prompt='Please enter the password for user ' + arguments["user"] + ": ", stream=None)
    else:
      print("Please use the user option to set the user account")
      sys.exit()

  
    if (arguments["url"] is None):
      raise NameError('Specify Ambari url (e.g. http://127.0.0.1:8080)')

    u = urlparse(arguments["url"])
    if ( len(u.scheme.strip()) == 0 or len(u.hostname.strip()) == 0 ) :
      raise NameError('Specify Ambari url (e.g. http://127.0.0.1:8080)')

    get_conf()

