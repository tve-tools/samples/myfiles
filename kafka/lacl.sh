#!/bin/bash

ZK=$1
topic=$2
[ "x$ZK" == "x" ] && echo 'please provide zk' && exit 0
[ "x$topic" == "x" ] && echo 'please provide topic' && exit 0

sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=$ZK:2181 --list --topic $topic
