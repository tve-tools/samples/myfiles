#!/bin/bash

sudo /usr/hdp/2.6.4.0-91/kafka/bin/kafka-topics.sh --zookeeper dbebpprdk8slx5834.fr.world.socgen:2181 --create --topic topic002 --partitions 1 --replication 2

sudo /usr/hdp/2.6.4.0-91/kafka/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=dbebpprdk8slx5834.fr.world.socgen:2181 --add --allow-principal User:X195076 --operation All --topic topic002

sudo /usr/hdp/2.6.4.0-91/kafka/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=dbebpprdk8slx5834.fr.world.socgen:2181 --list --topic topic002

sudo /usr/hdp/2.6.4.0-91/kafka/bin/kafka-acls.sh --authorizer-properties zookeeper.connect=dbebpprdk8slx5834.fr.world.socgen:2181 --add --allow-principal user:ANONYMOUS --operation All --topic topic002

sudo /usr/hdp/current/kafka-broker/bin/kafka-acls.sh --authorizer-properties  zookeeper.connect=dbebpprdk8slx5834.fr.world.socgen:2181 --add --allow-principal User:CN=prj01,OU=RESG,O=GROUPE_SOCIETE_GENERALE,L=Paris,ST=Ile_de_France,C=FR --operation Describe --topic topic002
