#!/bin/bash

ZK=$1
[ "x$ZK" == "x" ] && echo 'please provide zk' && exit 0

sudo /usr/hdp/current/kafka-broker/bin/kafka-topics.sh --zookeeper $ZK:2181 --list
