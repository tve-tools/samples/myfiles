#!/bin/bash

usage='sudo ~/ansible/reload.sh <backup_timestamp>'

backupdir=/bases/pgsql/dhadiha2/wal/ol-krb-backups
TS=$1

[ "z$TS" == "z" ] && echo $usage && exit 1
[ ! -r $backupdir/var-lib-ldap-$TS.zip ] && echo "check $backupdir/var-lib-ldap-$TS.zip" && exit 1
[ ! -r $backupdir/etc-openldap-$TS.zip ] && echo "check $backupdir/etc-openldap-$TS.zip" && exit 1
[ ! -r $backupdir/var-kerberos-$TS.zip ] && echo "check $backupdir/var-kerberos-$TS.zip" && exit 1

mydate="$(date +'%y%m%d-%H%M')"
#comment

systemctl stop slapd
systemctl stop kadmin
systemctl stop krb5kdc

cd /var/lib
mv ldap ldap-$mydate
unzip $backupdir/var-lib-ldap-$TS.zip
chown -R ldap:ldap ldap

cd /etc
mv openldap openldap-$mydate
unzip $backupdir/etc-openldap-$TS.zip
chown -R ldap:ldap openldap


cd /var
mv kerberos kerberos-$mydate
unzip $backupdir/var-kerberos-$TS.zip

#rm -f /var/kerberos/krb5kdc/kdc.conf; cp -p ~/kdc.conf.new /var/kerberos/krb5kdc/kdc.conf

systemctl start slapd
systemctl start kadmin
systemctl start krb5kdc


