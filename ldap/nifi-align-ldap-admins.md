```bash
cd ~/myfiles/ldap
./parse-ldif.py ~/n2.ldif X195076 |sort >X195076.out
./parse-ldif.py ~/n2.ldif A445139 |sort >A445139.out
sdiff -s A445139.out X195076.out >add-to-grps.ldif

vi 2.ldif
# :%s /\(.*\)/dn: uid=X195076,ou=Users,dc=haas,dc=socgen^Mchangetype: modify^Madd: memberOf^MmemberOf: \1^M^M/


dn: uid=X195076,ou=Users,dc=haas,dc=socgen
changetype: modify
add: memberOf
memberOf: cn=gnifi_d_power_amd,ou=Groups,dc=haas,dc=socgen



vi 3.ldif
# :%s /\(.*\)/dn: \1^Mchangetype: modify^Madd: member^Mmember: uid=X195076,ou=Users,dc=haas,dc=socgen^M^M/

dn: cn=gnifi_d_power_amd,ou=Groups,dc=haas,dc=socgen
changetype: modify
add: member
member: uid=X195076,ou=Users,dc=haas,dc=socgen


ldapmodify -H ldaps://dhadlx135.haas.socgen:636 -D uid=X195076,ou=Users,dc=haas,dc=socgen -W -f 2-lite.ldif
ldapmodify -H ldaps://dhadlx135.haas.socgen:636 -D uid=X195076,ou=Users,dc=haas,dc=socgen -W -f 2.ldif
ldapmodify -H ldaps://dhadlx135.haas.socgen:636 -D uid=X195076,ou=Users,dc=haas,dc=socgen -W -f 3-lite.ldif
ldapmodify -H ldaps://dhadlx135.haas.socgen:636 -D uid=X195076,ou=Users,dc=haas,dc=socgen -W -f 3.ldif

sudo slapcat -n2 -o ldif-wrap=no >~/n2.ldif
mv X195076.out X195076-init.out
mv A445139.out A445139-init.out
./parse-ldif.py ~/n2.ldif X195076 |sort >X195076.out
./parse-ldif.py ~/n2.ldif A445139 |sort >A445139.out
sdiff -s X195076.out A445139.out



```
