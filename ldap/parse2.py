#!/bin/python
import sys
from ldif import LDIFParser,LDIFWriter

#SKIP_DN = ["uid=foo,ou=People,dc=example,dc=com",
#   "uid=bar,ou=People,dc=example,dc=com"]

class MyLDIF(LDIFParser):
  def __init__(self,input,output):
    LDIFParser.__init__(self,input)
    self.writer = LDIFWriter(output)

  def handle(self,dn,entry):
#    print("entry: " + str(entry))
    flag = False
    for val in entry.values():
      for elem in val:
#        print('elem: ' + str(elem))
        if 'Manager' in elem:
          flag = True
    if flag:
      self.writer.unparse(dn,entry)
#      if dn in SKIP_DN:
#        return
     

#parser = MyLDIF(open("/home/x195076/n2.ldif", 'rb'), sys.stdout)
parser = MyLDIF(open("n2.ldif", 'rb'), sys.stdout)
parser.parse()
