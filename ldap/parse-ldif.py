#!/bin/python
# usage: ./parse-ldif.py ~/n2.ldif X195076
# usage: ./parse-ldif.py ~/n2.ldif A445139

import sys
from ldif import LDIFParser,LDIFWriter

class MyLDIF(LDIFParser):
  def __init__(self,input,output):
    LDIFParser.__init__(self,input)
    self.writer = LDIFWriter(output)

  def handle(self,dn,entry):
    flag = False

    for k, v in entry.items():
      if k not in ['member', 'uniqueMember']:
        continue

      for elem in v:
#        if 'A445139' in elem:
        if sys.argv[2] in elem:
          flag = True

    if flag:
      print(dn)
     

parser = MyLDIF(open(sys.argv[1], 'rb'), sys.stdout)
parser.parse()
