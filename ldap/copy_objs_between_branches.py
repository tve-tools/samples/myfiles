#!/usr/bin/python

# usage: ./copy_objs_between_branches.py -u ldaps://dbebpprdlx5265:636 -b cn=Manager,dc=haas,dc=socgen -w ***REMOVED***
# usage: ./copy_objs_between_branches.py -u ldaps://dbebpprdlx5265:636 -b cn=Manager,dc=haas,dc=socgen -w ***REMOVED*** -c

import ldap
import getopt
import sys
import getpass


LDAP_URL='ldaps://dbebpprdlx5265:636'
BIND='cn=Manager,dc=haas,dc=socgen'
PASSWD='***REMOVED***'

debug=True

USAGE = sys.argv[0] + ' { { -b | --bind } <ldap_bind_dn> } { { -u | --url } <ldap_url> } [ -c | --commit ] [ { -w | --passwd } <passwd> ]'

class Server:
  def __init__(self, url, bind, passwd):
    self.url = url
    self.bind = bind
    self.passwd = passwd
    l = ldap.initialize(self.url)
    l.simple_bind_s(self.bind, self.passwd)

    self.l = l

  def __str__(self):
    return "url: " + self.url + " - bind: " + self.bind

  def __del__(self):
    # not required to be called explicitly
    self.l.unbind()
    print("Unbind to " + str(self))

  def search(self, root, filtr, attrs):
    return self.l.search_s(root, ldap.SCOPE_SUBTREE, filtr, attrs)

  def add_to_grp(self, grp, mod_attrs):
    self.l.modify_s(grp, mod_attrs)



#ldap_server = Server(LDAP_URL, BIND, PASSWD)

try:
  options, remainder = getopt.getopt( sys.argv[1:], 'b:cu:w:', [ 'bind=', 'url=', 'commit', 'passwd=' ] )
except getopt.GetoptError:
  print USAGE
  exit(0)


for opt, arg in options:
  if opt in ('-u', '--url'):
    url = arg
  elif opt in ('-c', '--commit'):
    debug = False
  elif opt in ('-w', '--passwd'):
    passwd = arg
  elif opt in ('-b', '--bind'):
    bind = arg

try:
  bind
  url
except NameError:
  print USAGE
  exit(0)

try:
  passwd
except NameError:
  passwd = getpass.getpass()

ldap_server = Server(url, bind, passwd)

operator_users_search_result = []
operator_users_search_result = ldap_server.search('cn=gldap_operator,ou=Groups,dc=haas,dc=socgen', '(objectclass=*)', ['uniqueMember'])
operator_users = []
for record in operator_users_search_result:
  for unique_member in record[1]['uniqueMember']:
    operator_users.append(unique_member)


ldap_adds = []
power_users_search_result = ldap_server.search('cn=gldap_power,ou=Groups,dc=haas,dc=socgen', '(objectclass=*)', ['uniqueMember'])
for record in power_users_search_result:
  for unique_member in record[1]['uniqueMember']:
    if unique_member not in operator_users:
      print(unique_member + ' to be added to gldap_operator')
      if not debug: ldap_adds.append( (ldap.MOD_ADD, 'uniqueMember', unique_member ))

if ldap_adds: 
  ldap_server.add_to_grp(grp = 'cn=gldap_operator,ou=Groups,dc=haas,dc=socgen', mod_attrs = ldap_adds)


#member = [( ldap.MOD_ADD, 'uniqueMember', 'uid=X120365,ou=Users,dc=haas,dc=socgen' )]
#ldap_server.add_to_grp(grp = 'cn=gldap_operator,ou=Groups,dc=haas,dc=socgen', mod_attrs = member)


